import torch, torchani
import transformers as tfm
import dataloader as dl 
# import physnet_block as pb
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import MDAnalysis as mda
import tqdm
import tempfile
import pickle, json, os, copy
import gc
import collections
import networkx as nx
import yaml
import logging
import warnings
import wandb
from vae import VAE
from ae import AE
from pca_vae import PCA_VAE
from convVae import VanillaVAE
from twoStageVae import TwoStageVAE
from vaeNeuralODE import NeuralVAE as nVAE
from pca import PCA_torch
import argparse
from typing import *
from loss_utils import *
from curtsies import fmtfuncs as cf
import io, PIL
from plotly.subplots import make_subplots
import plotly.graph_objs as go
from wasserstein_utils import anneal
from captum.attr import Saliency
from md import refine as refine_mm
from dataloader import alignTrajectory
from openmm.app import *
from openmm import *
from openmm.unit import *

pl.seed_everything(42)

logging.basicConfig()
logger = logging.getLogger("model.py logger")
logger.setLevel(logging.DEBUG)

warnings.simplefilter("ignore")
#device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

class Model(pl.LightningModule):
    def __init__(self, args, **kwargs):
        super().__init__()
        
        self.args = args
        model_configs = kwargs.get("model_configs", None)
        if args.which_model in ["fc", "fc_wass"]:
            self.model_block = VAE(args, **model_configs) 
        if args.which_model == "fc_ae":
            self.model_block = AE(args, **model_configs)                 
        elif args.which_model == "vanilla":
            self.model_block = VanillaVAE(args, **model_configs)     
        elif args.which_model == "twostage":
            self.model_block = TwoStageVAE(args, **model_configs)     
        elif args.which_model == "neural":
            self.model_block = nVAE(args, **model_configs)   
        elif args.which_model == "pca":
            self.model_block = PCA_torch(**model_configs)   
        elif args.which_model == "pca_vae": 
            self.model_block = PCA_VAE(args,**model_configs) 
            
        self.beta = args.beta
        self.data_mean = None
        self.data_std = None
        self.loader_length = None
        
        if self.args.nolog:
            pass
        else:
            if self.args.user == "hyun":
                self.wandb_run = wandb.init(project="VAEGAN_MD", entity="hyunp2", name=args.name, group="DDP_runs")
            else:   
                self.wandb_run = wandb.init(project="VAEGAN_MD", entity="yifei6", name=args.name, group="DDP_runs")
            wandb.watch(self.model_block, log="all")
            os.environ["WANDB_CACHE_DIR"] = os.getcwd()
#         latent_dim = self.model_block.decoder.hidden_dims[0] #2, 3, etc.
#         self.column_names = column_names = [f"z{i}" for i in range(latent_dim)]
      
    def forward(self, coords):
        #WIP: add atom/res types!
        coords = coords.contiguous()
        if self.args.which_model == 'fc_ae':
            z, x = self.model_block(coords)
            mu = z
            return z, mu, z.new_zeros(z.size()), x
            #return z, 'no_mu', 'no_logstd', x
        else: 
            z, mu, logstd, x = self.model_block(coords) #x: reconstructed 
            # if args.which_model == 'pca_vae':
            #     pca = PCA_torch(**config) ##
            #     ckpt = torch.load(os.path.join(args.load_pcamodel_directory, args.load_pcamodel_checkpoint + ".pt"))
            #     for (key, _) in pca.named_buffers():
            #         setattr(pca, key, ckpt[key])    
            #     pca.to(self.dataset) ##
            #     x = pca.inverse_transform(x)
            return z, mu, logstd, x

    def saliency(self, coords):
        def forward_fn(coords):
            if self.args.which_model == 'fc_ae':
                z, x = self.model_block(coords)
                mu = z
            else:
                z, mu, logstd, x = self.model_block(coords)
            logprob = torch.distributions.Normal(0, 1).log_prob(mu).sum(dim=-1) #->(batch, )
            return logprob
        attrs = Saliency(forward_fn)
        coords = coords.detach().requires_grad_(True)
        assert coords.requires_grad
        # print(forward_fn(coords))
        saliency = attrs.attribute(coords) #->(B,3N)
        # print(saliency)
        saliency = saliency.view(coords.size(0), -1, 3).pow(2).sum(-1).sqrt() #->(B,N); normalized pseudo-forces
        saliency = saliency.mean(dim=0).detach().cpu().numpy() #->(N); num_residues
        # print(saliency)
        saliency = (saliency - min(saliency)) / (max(saliency) - min(saliency)) * 2 - 1
        pdb = os.path.join(self.args.load_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced.pdb") #string
        atom_selection = self.args.atom_selection
        u = mda.Universe(pdb) #universe
        mda_traj_name = os.path.join(self.args.save_data_directory, self.args.name + "_saliency.pdb") if self.args.name is not None else os.path.join(self.args.save_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced" + "_saliency.pdb")
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                resids = u.residues.resids
                resids = resids - min(resids) #indexing from 0!
                resids = np.repeat(resids, repeats=4) #[0,0,0,0,1,1,1,1]...
                sals = saliency[resids]
                assert len(sals) == len(resids), "Wrong elongation repeats!"
                u.add_TopologyAttr('occupancies', values=sals)
                w.write(u.atoms)   
        return self

    @property
    def refine(self, ):
        """takes in dcd trajectory and splits out PDBs which are minimized...
        MUST PARALLELIZE!!!
        For one PDB individually --> https://docs.mdanalysis.org/1.0.1/documentation_pages/coordinates/PDB.html#:~:text=In%20order%20to%20write%20a%20selection%20to%20a%20PDB%20file%20one%20typically%20uses%20the%20MDAnalysis.core.groups.AtomGroup.write()%20method%20of%20the%20selection%3A
        """
        prot_traj, prot_ref, atom_selection = alignTrajectory(self.args) #prot_traj MUST be a interpolated DCD! prot_ref MUST be a reduced PDB!!
        
        #BELOW: https://stackoverflow.com/questions/3223604/how-to-create-a-temporary-directory-and-get-its-path-file-name#:~:text=temp_dir%20%3D%20tempfile.TemporaryDirectory()%0Aprint(temp_dir.name)
        with tempfile.TemporaryDirectory() as tmpdirname:
            print(cf.on_yellow(f"Temporary directory {tmpdirname.name} has been created!!"))
            for ts in prot_traj.trajectory:
                prot_traj.select_atoms(atom_selection).write( os.path.join(tmpdirname.name, self.args.name + f"_{ts.frame}.pdb") if self.args.name is not None else os.path.join(tmpdirname.name, os.path.splitext(self.args.pdb_file)[0] + "_reduced" + f"_{ts.frame}.pdb") )
        
        pbar = tqdm.tqdm(os.walk(tmpdirname.name))
        pdbs_to_refine = [refine_mm.remote(os.path.join(root, fn)) for root, di, fn in pbar]
        ray.get(pdbs_to_refine) #List of Nones (we only need PDBs saved!)
        refined_pdbs = sorted([os.path.join(root, fn) for root, di, fn in os.walk(tmpdirname.name) if "fixed" in fn], key=lambda inp: int(inp.split("_")[-2]) ) #Get only refined (i.e. fixed PDBs); SORTED!

        #Below is OPENMM
        @ray.remote
        def get_PE(pdb_file: str):
            pdb = PDBFile(pdb_file)
        #     force_field = openmm.app.ForceField("amber14/protein.ff14SB.xml") charmm36.xml
            force_field = openmm.app.ForceField("charmm36.xml") 
            modeller = openmm.app.Modeller(pdb.topology, pdb.positions)
            modeller.addHydrogens(force_field)
            system = force_field.createSystem(modeller.topology, nonbondedMethod=PME,
                                                nonbondedCutoff=1*nanometer, constraints=HBonds)
            integrator = openmm.LangevinIntegrator(0, 0.01, 1.0)
            simulation = Simulation(modeller.topology, system, integrator)
            simulation.context.setPositions(modeller.positions)
            state = simulation.context.getState(getEnergy=True) #http://docs.openmm.org/latest/userguide/application/03_model_building_editing.html#model-building-and-editing
            energy = state.getPotentialEnergy() / unit.kilocalories_per_mole
            print(energy) #https://github.com/openmm/openmm/issues/1979
            return energy
            
        pbar = tqdm.tqdm(refined_pdbs) #Already with directories!
        pdb_PEs = [get_PE.remote(pdb_file) for pdb_file in pbar]
        pdb_energies = ray.get(pdb_PEs)
        print("Potential energy in kcal-per-mole: ", pdb_energies)
        print(cf.on_yellow("Done!"))

        #Below is optional to save full-atom DCD
        u = mda.Universe(refined_pdbs[0], *refined_pdbs) #need refined_pdbs[0] for topology; *refined_pdbs for trajs
        mda_traj_name = os.path.join(self.args.save_data_directory, self.args.name + "_fullatom.dcd") if self.args.name is not None else os.path.join(self.args.save_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_fullatom.dcd")
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                w.write(u.atoms)   

        tmpdirname.cleanup() #Remove temporary directory


    def _shared_step(self, batch, batch_idx, return_metadata=False):
        batch = batch.contiguous()
        coords = batch
        print(f"-------------this batch size{batch.shape}---------------------------")
        z, mu, logstd, x = self(coords) #coords: BL3 -> z: (B,latdim)
        assert self.data_mean != None and self.data_std != None, "initialize mean and std correctly!"
        mse, kl, wass, rmsd, gdt, tm  = self.model_block.losses(coords, z, mu, logstd, x, self.args.beta, self.data_mean.to(coords), self.data_std.to(coords)) #beta-VAE
        if self.args.which_model == "neural":
            x, logp = x
            tm, logp = tm
            tm = (tm, logp)
        total_training_steps = self.loader_length * self.args.num_epochs
        if self.args.which_model == "fc_wass":
            BETA = 0.001
            WBETA = anneal(start_val=1.0, end_val=2.0, start_iter=0, end_iter=total_training_steps, it=self.global_step)
            WBETA = self.args.wbeta * WBETA
        else: 
            BETA = 1
            WBETA = 0
        ###LOSSES are ALREADY REWEIGHTED!
        if not return_metadata:
            return mse, BETA*kl, WBETA*wass, rmsd, gdt, tm
        else:
            return mse, BETA*kl, WBETA*wass, rmsd, gdt, tm, z, mu, logstd, x
        
    def on_train_epoch_start(self, ) -> None:
        print(f"Current epoch is {self.current_epoch}!")
        
    def training_step(self, batch, batch_idx):
        # batch = batch[0]
        mse, kl, wass, rmsd, gdt, tm = self._shared_step(batch, batch_idx)
        
        if self.args.which_model == "neural":
            tm, logp = tm

            if not self.args.nolog:
                wandb.log({
                        'train_kl': kl.mean().item(),
                        'train_mse': mse.mean().item(),
                        'train_wass': wass.mean().item(),
                        'train_rmsd': rmsd.mean().item(),
                        'train_gdt': gdt.mean().item(),
                        'train_tm': tm.mean().item(),
                        'train_logp': logp.mean().item()
                        })
        else:
            if not self.args.nolog:
                wandb.log({
                        'train_kl': kl.mean().item(),
                        'train_mse': mse.mean().item(),
                        'train_wass': wass.mean().item(),
                        'train_rmsd': rmsd.mean().item(),
                        'train_gdt': gdt.mean().item(),
                        'train_tm': tm.mean().item(),
                        })

        loss = (mse + kl + wass + rmsd - gdt - tm) if not (self.args.which_model == "neural") else (mse + kl + wass + rmsd - gdt - tm + logp)
        loss = loss.mean()
        self.log("train_loss", loss, prog_bar=True)

        if self.args.which_model == "neural":
            return {"loss": loss, "train_kl": kl.mean(), "train_mse": mse.mean(), "train_wass": wass.mean(), "train_rmsd": rmsd.mean(), "train_gdt": gdt.mean(), "train_tm": tm.mean(), "train_logp": logp.mean()}
        else:
            return {"loss": loss, "train_kl": kl.mean(), "train_mse": mse.mean(), "train_wass": wass.mean(), "train_rmsd": rmsd.mean(), "train_gdt": gdt.mean(), "train_tm": tm.mean()}

    def training_epoch_end(self, training_step_outputs):
#         if not self.trainer.sanity_checking:
        epoch_train_loss = torch.stack([x["loss"] for x in training_step_outputs]).mean()
        epoch_train_mse = torch.stack([x["train_mse"] for x in training_step_outputs]).mean()
        epoch_train_kl = torch.stack([x["train_kl"] for x in training_step_outputs]).mean()
        epoch_train_wass = torch.stack([x["train_wass"] for x in training_step_outputs]).mean()
        epoch_train_rmsd = torch.stack([x["train_rmsd"] for x in training_step_outputs]).mean()
        epoch_train_gdt = torch.stack([x["train_gdt"] for x in training_step_outputs]).mean()
        epoch_train_tm = torch.stack([x["train_tm"] for x in training_step_outputs]).mean()
        if self.args.which_model == "neural":
            epoch_train_logp = torch.stack([x["train_logp"] for x in training_step_outputs]).mean()

        self.log("epoch_train_loss", epoch_train_loss)
        if self.args.which_model == "neural":
            if not self.args.nolog:
                wandb.log({'epoch': self.current_epoch,
                        'epoch_lr': self.trainer.optimizers[0].param_groups[0]["lr"],
                        'epoch_train_loss': epoch_train_loss,
                        'epoch_train_mse': epoch_train_mse,
                        'epoch_train_kl': epoch_train_kl,
                        'epoch_train_rmsd': epoch_train_rmsd,
                        'epoch_train_gdt': epoch_train_gdt,
                        'epoch_train_tm': epoch_train_tm,
                        'epoch_train_logp': epoch_train_logp})
        else:
            if not self.args.nolog:
                wandb.log({'epoch': self.current_epoch,
                        'epoch_lr': self.trainer.optimizers[0].param_groups[0]["lr"],
                        'epoch_train_loss': epoch_train_loss,
                        'epoch_train_mse': epoch_train_mse,
                        'epoch_train_kl': epoch_train_kl,
                        'epoch_train_wass': epoch_train_wass,
                        'epoch_train_rmsd': epoch_train_rmsd,
                        'epoch_train_gdt': epoch_train_gdt,
                        'epoch_train_tm': epoch_train_tm})

#     @staticmethod
#     def plot_manifold(args: argparse.ArgumentParser, mus: Union[np.ndarray, torch.Tensor], logstds: Union[np.ndarray, torch.Tensor], title: str, nologging=False):
#         #WIP for PCA or UMAP or MDS
#         #summary is 
#         import sklearn.manifold
#         import plotly.express as px
#         from umap import UMAP
#         import scipy.stats
# #         proj = sklearn.manifold.TSNE(2)
#         proj = UMAP(random_state=42)
#         mus_proj = proj.fit_transform(mus) #(B,2) of tsne
#         path_to_plotly_html = os.path.join(args.save_data_directory, "plotly_figure.html")
#         dist = scipy.stats.multivariate_normal(np.zeros(mus.shape[1]), 1)
#         fig = px.scatter(x=mus_proj[:,0], y=mus_proj[:,1], color=dist.pdf(mus).reshape(-1,))
#         fig.write_html(path_to_plotly_html, auto_play = False)
#         if not nologging:
#         table = wandb.Table(columns = ["plotly_figure"])
#         table.add_data(wandb.Html( open(path_to_plotly_html) ))
#         wandb.log({f"{proj.__class__.__name__} Plot {title}": table}) #Avoids overlap!
#         return proj #Fitted 

    @staticmethod
    def plot_manifold(args: argparse.ArgumentParser, mus: Union[np.ndarray, torch.Tensor], logstds: Union[np.ndarray, torch.Tensor], title: str):
        #WIP for PCA or UMAP or MDS
        #summary is 
        import plotly.express as px
        import scipy.stats
        
        assert len(mus.shape) == 2 and len(logstds.shape) == 2
        path_to_plotly_html = os.path.join(args.save_data_directory, "plotly_figure.html")
        dist = scipy.stats.multivariate_normal(np.zeros(mus.shape[1]), 1)
        if not args.nolog:
            table = wandb.Table(columns = ["plotly_figure"])
        colors = [mus, logstds, np.arange(mus.shape[0])]

        if mus.shape[1] == 2:
            for i, c in enumerate(colors):
                if i == 0:
                    fig = px.scatter(x=mus[:,0], y=mus[:,1], color=dist.pdf(c).reshape(-1,)) 
                elif i == 1:
                    fig = px.scatter(x=mus[:,0], y=mus[:,1], color=np.exp(c.sum(axis=-1)).reshape(-1,)) 
                elif i == 2:
                    fig = px.scatter(x=mus[:,0], y=mus[:,1], color=c.reshape(-1,)) 
                fig.write_html(path_to_plotly_html, auto_play = False)
                fig.write_image(os.path.splitext(path_to_plotly_html)[0] + f"_labels_{i}.png")

                if not args.nolog: table.add_data(wandb.Html( open(path_to_plotly_html) ))
        elif mus.shape[1] >= 3:
            for i, c in enumerate(colors):
                if i == 0:
                    fig = px.scatter_3d(x=mus[:,0], y=mus[:,1], z=mus[:,2], color=dist.pdf(c).reshape(-1,)) 
                elif i == 1:
                    fig = px.scatter_3d(x=mus[:,0], y=mus[:,1], z=mus[:,2], color=np.exp(c.sum(axis=-1)).reshape(-1,)) 
                elif i == 2:
                    fig = px.scatter_3d(x=mus[:,0], y=mus[:,1], z=mus[:,2], color=c.reshape(-1,)) 
                fig.write_html(path_to_plotly_html, auto_play = False)
                fig.write_image(os.path.splitext(path_to_plotly_html)[0] + f"_logprob_{i}.png")

                if not args.nolog: table.add_data(wandb.Html( open(path_to_plotly_html) ))
        # elif mus.shape[1] > 3:
        #     print(cf.on_yellow("Choosing first two axes by force..."))
        #     for i, c in enumerate(colors):
        #         if i == 0:
        #             fig = px.scatter(x=mus[:,0], y=mus[:,1], color=dist.pdf(c).reshape(-1,)) 
        #         elif i == 1:
        #             fig = px.scatter(x=mus[:,0], y=mus[:,1], color=np.exp(c.sum(axis=-1)).reshape(-1,)) 
        #         elif i == 2:
        #             fig = px.scatter(x=mus[:,0], y=mus[:,1], color=c.reshape(-1,)) 
        #         fig.write_html(path_to_plotly_html, auto_play = False)
        #         table.add_data(wandb.Html( open(path_to_plotly_html) ))

#         wandb.log({f"{proj.__class__.__name__} Plot {title}": table}) #Avoids overlap!
        if not args.nolog: 
            wandb.log({f"Latent Plot {title}": table})
        #np.save(os.path.join(args.save_data_directory, f"epoch_{title}_latent.npy"), mus)
        np.savez(os.path.join(args.save_data_directory, f"manifold_latent_{args.name}.npz"), mus = mus, logstds = logstds)
#         return proj #Fitted 

    @staticmethod
    def plot_manifold_with_colors(args: argparse.ArgumentParser, mus: Union[np.ndarray, torch.Tensor], logstds: Union[np.ndarray, torch.Tensor], title: str, colors: Union[np.ndarray, torch.Tensor], get_logpdf=False, **kwargs):
        #WIP for PCA or UMAP or MDS
        #summary is 
        import plotly.express as px
        import scipy.stats
        
        assert len(mus.shape) == 2 and len(logstds.shape) == 2
        path_to_plotly_html = os.path.join(args.save_data_directory, "plotly_figure.html")
#         dist = scipy.stats.multivariate_normal(np.zeros(mus.shape[1]), 1)

        if not args.nolog:
            table = wandb.Table(columns = ["plotly_figure"])

        fig = go.Figure()
        if mus.shape[1] is not None:
            # fig = px.scatter(x=mus[:,0], y=mus[:,1], color=colors.reshape(-1,)) 
            # fig.add_trace(go.Scatter(x=mus[:,0], y=mus[:,1], marker=dict(color=colors.reshape(-1,)), mode="markers" ))
            unique_colors = np.unique(colors) #0, and 100*noises
            for idx, uc in enumerate(unique_colors):
                c = colors[uc == colors]
                if idx == 0:
                    fig.add_trace(go.Scatter(x=mus[:,0][uc == colors], y=mus[:,1][uc == colors], marker=dict(color=[10]*c.shape[0], colorbar=dict(thickness=10)), mode="markers" ,                             hovertemplate='<i>Name</i>: %{text}',
                                text = c , opacity=0.5))
                else:
                    fig.add_trace(go.Scatter(x=mus[:,0][uc == colors], y=mus[:,1][uc == colors], marker=dict(color=np.arange(c.shape[0]), colorbar=dict(thickness=10)), mode="lines+markers",                             hovertemplate='<i>Name</i>: %{text}',
                                text = c ,))

        # elif mus.shape[1] >= 3:
        #     # fig = px.scatter_3d(x=mus[:,0], y=mus[:,1], z=mus[:,2], color=colors.reshape(-1,)) 
        #     unique_colors = np.unique(colors) #0, and 100*noises
        #     for idx, uc in enumerate(unique_colors):
        #         c = colors[uc == colors]
        #         if idx == 0:
        #             fig.add_trace(go.Scatter3d(x=mus[:,0][uc == colors], y=mus[:,1][uc == colors], z=mus[:,2][uc == colors], marker=dict(color=[idx]*c.shape[0]), mode="markers" ,                             hovertemplate='<i>Name</i>: %{text}',
        #                         text = c , opacity=0.5))
        #         else:
        #             fig.add_trace(go.Scatter3d(x=mus[:,0][uc == colors], y=mus[:,1][uc == colors], z=mus[:,2][uc == colors], marker=dict(color=[idx]*c.shape[0]), mode="lines+markers",                             hovertemplate='<i>Noise Level</i>: %{text}',
        #                         text = np.arange(len(c)) ,))
                                # text = c / 1e3 ,))
        # elif mus.shape[1] > 3:
        #     print(cf.on_yellow("Choosing first two axes by force..."))
        #     fig = px.scatter(x=mus[:,0], y=mus[:,1], color=colors.reshape(-1,))
        # fig.update_layout(coloraxis=dict(colorbar=dict()))
        fig.write_html(path_to_plotly_html, auto_play = False)
        fig.write_image(os.path.splitext(path_to_plotly_html)[0] + "_labels_noise.png")
        if not args.nolog: table.add_data(wandb.Html( open(path_to_plotly_html) ))

        fig = go.Figure()
        if not get_logpdf:
            if mus.shape[1] == 2:
                fig = px.scatter(x=mus[:,0], y=mus[:,1], color=np.arange(mus.shape[0]).reshape(-1,)) 
            elif mus.shape[1] >= 3:
                fig = px.scatter_3d(x=mus[:,0], y=mus[:,1], z=mus[:,2], color=np.arange(mus.shape[0]).reshape(-1,)) 
        else:
            logpdf_colors = kwargs.get("logpdf_colors", np.zeros(mus.shape[0]))
            unique_colors = np.unique(logpdf_colors)[::-1] #0, and logprobs<0
            noise_level_uniques = np.unique(colors)
            print(unique_colors, noise_level_uniques)
            if mus.shape[1] == 2:
                # fig = px.scatter(x=mus[:,0], y=mus[:,1], color=logpdf_colors.reshape(-1,) ) 
                fig.add_trace(go.Scatter(x=mus[:,0], y=mus[:,1], marker=dict(color=logpdf_colors.reshape(-1,)), mode="markers" ))
            elif mus.shape[1] >= 3:
                # fig = px.scatter_3d(x=mus[:,0], y=mus[:,1], z=mus[:,2], color=logpdf_colors.reshape(-1,)) 
                for idx, (u, uc) in enumerate(zip(noise_level_uniques, unique_colors)):
                    c = logpdf_colors[uc == logpdf_colors]
                    customdata = colors[u == colors]
                    if idx == 0:
                        fig.add_trace(go.Scatter3d(x=mus[:,0][uc == logpdf_colors], y=mus[:,1][uc == logpdf_colors], z=mus[:,2][uc == logpdf_colors], marker=dict(color=[idx]*c.shape[0]), mode="markers" ,                            hovertemplate='<i>Name</i>: %{text}',                              text = c , opacity=0.5))
                    else:
                        print(u)
                        fig.add_trace(go.Scatter3d(x=mus[:,0][uc == logpdf_colors], y=mus[:,1][uc == logpdf_colors], z=mus[:,2][uc == logpdf_colors], marker=dict(color=[idx]*c.shape[0], line=dict(width=6)), mode="lines+markers" ,                            hovertemplate='<i>Log Prob of Path</i>: %{text}<br><b>Noise Level</b>: %{customdata:.3f}<br>',                              text = c , customdata=customdata / 1e3))

        # elif mus.shape[1] > 3:
        #     print(cf.on_yellow("Choosing first two axes by force..."))
        #     fig = px.scatter(x=mus[:,0], y=mus[:,1], color=colors.reshape(-1,))
        fig.write_html(path_to_plotly_html, auto_play = False)
        fig.write_image(os.path.splitext(path_to_plotly_html)[0] + "_logprob_noise.png")
        if not args.nolog:
            table.add_data(wandb.Html( open(path_to_plotly_html) ))
            wandb.log({f"Latent Plot with Interps": table})

    def on_validation_epoch_start(self, ) -> None:
#         self.wandb_table = wandb.Table(columns=self.column_names)
#         self.df = []
#         print("Validation starts...")
        pass
        
    def validation_step(self, batch, batch_idx):
        # batch = batch[0]
        batch = batch.contiguous()
        # print(batch.shape)
        mse, kl, wass, rmsd, gdt, tm, z, mu, logstd, x = self._shared_step(batch, batch_idx, return_metadata=True)
        if self.args.which_model == "neural":
            tm, logp = tm
        if not self.args.nolog:
            wandb.log({
                    'val_kl': kl.mean().item(),
                    'val_mse': mse.mean().item(),
                    'val_wass': wass.mean().item(),
                    })
        loss = (mse + kl + wass + rmsd - gdt - tm) if not (self.args.which_model == "neural") else (mse + kl + wass + rmsd - gdt - tm + logp)
        loss = loss.mean()
        self.log("val_loss", loss, prog_bar=True)
        return {"val_loss": loss, "val_kl": kl.mean(), "val_mse": mse.mean(), "val_wass": wass.mean(), "mu": mu, "logstd": logstd}

    def validation_epoch_end(self, validation_step_outputs):
#         if not self.trainer.sanity_checking:
        epoch_val_loss = torch.stack([x["val_loss"] for x in validation_step_outputs]).mean()
        epoch_val_mse = torch.stack([x["val_mse"] for x in validation_step_outputs]).mean()
        epoch_val_kl = torch.stack([x["val_kl"] for x in validation_step_outputs]).mean()
        epoch_val_wass = torch.stack([x["val_wass"] for x in validation_step_outputs]).mean()

        mus = torch.cat([x["mu"] for x in validation_step_outputs], dim=0) #(b,dim) -> (B,dim)
        logstds = torch.cat([x["logstd"] for x in validation_step_outputs], dim=0) #(b,dim) -> (B,dim)
        self.log("epoch_val_loss", epoch_val_loss)
        self.log("epoch_val_mse", epoch_val_mse)
        self.log("epoch_val_kl", epoch_val_kl)
        self.log("epoch_val_wass", epoch_val_wass)

        if not self.args.nolog:
            wandb.log({
                    'epoch_val_loss': epoch_val_loss,
                    'epoch_val_mse': epoch_val_mse,
                    'epoch_val_kl': epoch_val_kl,
                    "epoch_val_wass": epoch_val_wass
            })
        if self.current_epoch % 100 == 0:
            #WIP: Change modulus!
            print(mus.shape, logstds.shape)
            self.plot_manifold(self.args, mus.detach().cpu().numpy(), logstds.detach().cpu().numpy(), self.current_epoch)
#         df = torch.cat(self.df) #(MultiB, latent_dim)
#         self.wandb_table.add_data(*df.T)
#         wandb.run.log({f"Epoch {self.current_epoch} Valid Latent Representation" : wandb.plot.scatter(self.wandb_table,
#                             *self.column_names)})
    
    
    def on_test_epoch_start(self, ) -> None:
        print("Testing starts...")
    
    def test_step(self, batch, batch_idx):
        # batch = batch[0]
        batch = batch.contiguous()
        mse, kl, wass, rmsd, gdt, tm, z, mu, logstd, x = self._shared_step(batch, batch_idx, return_metadata=True)
        if self.args.which_model == "neural":
            tm, logp = tm
        if not self.args.nolog:
            wandb.log({
                    'test_kl': kl.mean().item(),
                    'test_mse': mse.mean().item(),
                    'test_wass': wass.mean().item(),
                    })
        loss = (mse + kl + wass + rmsd - gdt - tm) if not (self.args.which_model == "neural") else (mse + kl + wass + rmsd - gdt - tm + logp)
        loss = loss.mean()
        self.log("test_loss", loss, prog_bar=True)
        return {"test_loss": loss, "test_kl": kl.mean(), "test_mse": mse.mean(), "test_wass": wass.mean(), "mu": mu, "logstd": logstd}

    def test_epoch_end(self, test_step_outputs):
#         if not self.trainer.sanity_checking:
        epoch_test_loss = torch.stack([x["test_loss"] for x in test_step_outputs]).mean()
        epoch_test_mse = torch.stack([x["test_mse"] for x in test_step_outputs]).mean()
        epoch_test_kl = torch.stack([x["test_kl"] for x in test_step_outputs]).mean()
        epoch_test_wass = torch.stack([x["test_wass"] for x in test_step_outputs]).mean()
        mus = torch.cat([x["mu"] for x in test_step_outputs], dim=0) #(b,dim) -> (B,dim)
        logstds = torch.cat([x["logstd"] for x in test_step_outputs], dim=0) #(b,dim) -> (B,dim)
        self.log("epoch_test_loss", epoch_test_loss)
        if not self.args.nolog:
            wandb.log({
                    'epoch_test_loss': epoch_test_loss,
                    'epoch_test_mse': epoch_test_mse,
                    'epoch_test_kl': epoch_test_kl,
                    'epoch_test_wass': epoch_test_wass,
            })
        self.plot_manifold(self.args, mus.detach().cpu().numpy(), logstds.detach().cpu().numpy(), self.current_epoch)
        return (mus.detach().cpu().numpy(), logstds.detach().cpu().numpy())
#     def on_predict_epoch_start(self, ):
#         #Called per EPOCH!
#         self.wandb_table = wandb.Table(columns=self.column_names)
#         self.df = []

#     def predict_step(self, batch, batch_idx, dataloader_idx=0):
#         generate_molecules
        
#     def on_predict_epoch_end(self, ):
#         #Called per EPOCH!
#         df = torch.cat(self.df) #(MultiB, latent_dim)
#         self.wandb_table.add_data(*df.T)
#         wandb.run.log({"Pred Latent Representation" : wandb.plot.scatter(self.wandb_table,
#                             *self.column_names)})

    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(self.model_block.parameters(), lr=self.args.lr)
        total_training_steps = self.loader_length * self.args.num_epochs
        warmup_steps = total_training_steps // self.args.warm_up_split
        if self.args.scheduler == "cosine":
            scheduler = tfm.get_cosine_with_hard_restarts_schedule_with_warmup(optimizer, num_warmup_steps = warmup_steps, num_training_steps=total_training_steps, num_cycles = 1, last_epoch = -1)
            scheduler = {"scheduler": scheduler, "interval": "step", "frequency": 1} #Every step/epoch with Frequency 1etc by monitoring val_loss if needed
            return [optimizer], [scheduler]
        elif self.args.scheduler == "linear":
            scheduler = tfm.get_linear_schedule_with_warmup(optimizer, num_warmup_steps = warmup_steps, num_training_steps=total_training_steps, last_epoch = -1)
            scheduler = {"scheduler": scheduler, "interval": "step", "frequency": 1} #Every step/epoch with Frequency 1etc by monitoring val_loss if needed
            return [optimizer], [scheduler]
        elif self.args.scheduler == "reduce":
            scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08, verbose=False)
            scheduler = {"scheduler": scheduler, "interval": "epoch", "frequency": 1, "monitor": "epoch_val_loss"} #Every step/epoch with Frequency 1etc by monitoring val_loss if needed
            return [optimizer], [scheduler]
        elif self.args.scheduler in [None, "none", "None"]:
            return optimizer



    def lerp(self, start: "1D tensor of features", end: "1D tensor of features", t: "1D tensor of weights"=1):
        outs = start + (end - start) * t.view(-1,1).to(self.device)
        return outs #(num_interpolations, dim)
        
    def geometric_slerp(start, end, t):
        #https://github.com/scipy/scipy/blob/v1.9.0/scipy/spatial/_geometric_slerp.py#L35-L238:~:text=def%20geometric_slerp(,.ndarray%3A
        
        #WIP!
        
        # create an orthogonal basis using QR decomposition
        # One data point and interpolated!
        start = start.view(1,-1) 
        end = end.view(1,-1)
        basis = torch.cat([start,end], dim=0) #shape: (2,dim) #np.vstack([start, end])
        Q, R = torch.qr(basis.t()) ###SAME as reduced option of np.linalg.qr;;;; transpose -> (dim,2) --> BREAKS down into: (dim,k) and (k,dim)
        signs = 2 * (torch.diag(R) >= 0) - 1
        Q = Q.t() * signs.t()[:, None]
        R = R.t() * signs.t()[:, None]

        # calculate the angle between `start` and `end`
        c = start.view(-1,).dot(end.view(-1,))
        s = torch.linalg.det(R)
        omega = torch.atan2(s, c)

        # interpolate
        start, end = Q
        s = torch.sin(t * omega)
        c = torch.cos(t * omega)
        return start * c[:, None] + end * s[:, None]
    
    def slerp(self, start, end, t, DOT_THRESHOLD=0.9995):
        '''
        https://github.com/PDillis/stylegan2-fun/blob/master/run_generator.py
        Spherical linear interpolation
        Args:
            t (float/np.ndarray): Float value between 0.0 and 1.0
            v0 (np.ndarray): Starting vector
            v1 (np.ndarray): Final vector
            DOT_THRESHOLD (float): Threshold for considering the two vectors as
                                    colineal. Not recommended to alter this.
        Returns:
            v2 (np.ndarray): Interpolation vector between v0 and v1
        '''
        # Copy the vectors to reuse them later
        v0_copy = start.clone()
        v1_copy = end.clone()
        # Normalize the vectors to get the directions and angles
        # v0 = v0 / np.linalg.norm(v0)
        # v1 = v1 / np.linalg.norm(v1)
        v0 = torch.nn.functional.normalize(start, dim=0)
        v1 = torch.nn.functional.normalize(end, dim=0)
        # Dot product with the normalized vectors (can't use np.dot in W)
        # dot = np.sum(v0 * v1)
        dot = v0.dot(v1)
        # If absolute value of dot product is almost 1, vectors are ~colineal, so use lerp
        if torch.abs(dot) > DOT_THRESHOLD:
            print(cf.on_yellow("Reverting to LERP, due to colinearity"))
            return self.lerp(v0_copy, v1_copy, t)
        # Calculate initial angle between v0 and v1
        theta_0 = dot.acos() #float
        sin_theta_0 = theta_0.sin() #float
        # Angle at timestep t
        theta_t = theta_0 * t #(num_interps)
        sin_theta_t = theta_t.sin() #(num_interps)
        # Finish the slerp algorithm
        s0 = (theta_0 - theta_t).sin() / sin_theta_0 #num_interps
        s1 = sin_theta_t / sin_theta_0 #(num_interps)
        v2 = s0[:, None] * v0_copy + s1[:, None] * v1_copy #(num_interps, dim)
        return v2
    
    def generate_molecules(self, test_loader: "test loader original coordinates (BL3)", start_idx: "starting point index, integer", end_idx: "end point index, integer", num_interps: "num of interpolations points", reference: "coord of reference", min=None, max=None, return_intermediates=False) -> "Original && Recon_from_original && Lerp":
        """MOVE to pl.Callback!
        WIP: Add Plotly functions!"""

        original_unscaled_list, recon_unscaled_list, lerps_recon_unscaled_list = [], [], []
        mu_list, logstd_list, color_list, mu_orig_list = [], [], [], []
        len_of_testset = len(test_loader.dataset)

        for i, original in enumerate(test_loader):
            original = original.to(self.device)
            z, mu, logstd, x = self(original) #"latend_coordinates of (B,latent_dim)"
            # mu = mu + logstd.exp() * torch.normal(0, 1, size=mu.size()).to(mu) #sampling_noise 0 makes mu as mu...

            ##BELOW: WIP for Interpolations!
            if i == 0: 
                inputs_for_later = mu[start_idx] # dim,
            elif (i == (len(test_loader) - 1)) or (len(test_loader) == 1):
                outputs_for_later = mu[end_idx] # dim,
                if self.args.lerp == "linear":
                    lerps_for_later = self.lerp(start=inputs_for_later, end=outputs_for_later, t=torch.linspace(0, 1, num_interps)[:]) #(Num_interpolations by latent_dim)
                elif self.args.lerp == "geometric":
                    lerps_for_later = self.slerp(start=inputs_for_later, end=outputs_for_later, t=torch.linspace(0, 1, num_interps)[:]) #(Num_interpolations by latent_dim)
            
            inputs = mu[start_idx] # dim,
            outputs = mu[end_idx] # dim,
            if self.args.lerp == "linear":
                lerps = self.lerp(start=inputs, end=outputs, t=torch.linspace(0, 1, num_interps)[1:-1]) #(Num_interpolations by latent_dim)
            elif self.args.lerp == "geometric":
                lerps = self.slerp(start=inputs, end=outputs, t=torch.linspace(0, 1, num_interps)[1:-1]) #(Num_interpolations by latent_dim)

            mean = self.data_mean #make sure dmo is saved as an argparse
            std = self.data_std
            unnormalize = dl.ProteinDataset.unnormalize if self.args.which_model in ["fc","fc_ae","fc_wass","pca_vae"] else dl.ProteinDatasetDistogram.unnormalize #static method

            # print(original.size(), mean.size(), std.size())

            original_unscaled = unnormalize(original, mean=mean, std=std, min=min, max=max, index=None)
            recon_scaled = self.model_block.decoder(mu) if self.args.which_model in ["fc","fc_ae","fc_wass","pca_vae"] else self.model_block.decode(mu) #BL3, (scaled coord)
            # print(original.shape, recon_scaled.shape, mu.shape, mean.shape, std.shape)

            recon_unscaled  = unnormalize(recon_scaled, mean=mean, std=std, min=min, max=max, index=None) #BL3, test_loader latent to reconstruction (raw coord)
            lerps_recon_scaled = self.model_block.decoder(lerps.to(mu)) if self.args.which_model in ["fc","fc_ae","fc_wass","pca_vae"] else self.model_block.decode(lerps.to(mu)) #BL3, lerped to reconstruction (scaled coord)
            lerps_recon_unscaled  = unnormalize(lerps_recon_scaled, mean=mean, std=std, min=min, max=max, index=None) #BL3, test_loader latent to reconstruction (raw coord)
    #         print(original_unscaled, recon)
            # colors = torch.cat([torch.LongTensor([i*10] * tensor.size(0)) for i, tensor in enumerate([original, recon_scaled]) ], dim=0).detach().cpu().numpy()
            colors = torch.cat([torch.LongTensor([10] * tensor.size(0)) for i, tensor in enumerate([original]) ], dim=0).detach().cpu().numpy()
            # traj_cats = torch.cat([original, recon_scaled], dim=0) #.detach().cpu().numpy() #BBB,L,3
            traj_cats = torch.cat([original], dim=0) #.detach().cpu().numpy() #BBB,L,3
            _, mus, logstds, _ = self(traj_cats)

            mu_orig_list.append(mus[:original.size(0)])
            mu_list.append(mus)
            logstd_list.append(logstds)
            color_list.append(colors)

    #         psf = self.args.psf_file
            pdb = os.path.join(self.args.load_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced.pdb") #string
            atom_selection = self.args.atom_selection

            # print("W&B is done...")

            if self.args.which_model in ["vanilla", "twostage"]:
                # Xs, Ys, Zs = [], [], []

                reference = reference.to("cpu") #1L3; must permute!
                original_unscaled = original_unscaled.to("cpu")
                recon_unscaled = recon_unscaled.to("cpu")
                lerps_recon_unscaled = lerps_recon_unscaled.to("cpu")

                original_unscaled = mds_torch(original_unscaled.permute(0,2,1).contiguous())[0] #->B3L
                recon_unscaled = mds_torch(recon_unscaled.permute(0,2,1).contiguous())[0] #->B3L
                lerps_recon_unscaled = mds_torch(lerps_recon_unscaled.permute(0,2,1).contiguous())[0] #->B3L
                print("MDS done!")

                X = original_unscaled #->B3L
                Y = recon_unscaled #->B3L
                Z = lerps_recon_unscaled #->B3L
                original_unscaled, _ = kabsch_torch(X, reference.permute(0,2,1).contiguous().expand_as(X)) #-> (B,3,L) alignment: translated and rotated to fit!
                recon_unscaled, _ = kabsch_torch(Y, reference.permute(0,2,1).contiguous().expand_as(X)) #-> (B,3,L) alignment: translated and rotated to fit!
                lerps_recon_unscaled, _ = kabsch_torch(Z, reference.permute(0,2,1).contiguous().expand_as(X)) #-> (B,3,L) alignment: translated and rotated to fit!
                print("Kabsch done!")

                original_unscaled = original_unscaled.permute(0,2,1).contiguous() #Back to BL3
                recon_unscaled = recon_unscaled.permute(0,2,1).contiguous()
                lerps_recon_unscaled = lerps_recon_unscaled.permute(0,2,1).contiguous()

            original_unscaled_list.append(original_unscaled)
            recon_unscaled_list.append(recon_unscaled)
            lerps_recon_unscaled_list.append(lerps_recon_unscaled)
        
        mus_orig = torch.cat(mu_orig_list, dim=0) #only for orig mus
        mus = torch.cat(mu_list, dim=0)
        logstds = torch.cat(logstd_list, dim=0)
        colors = np.concatenate(color_list, axis=0)

        original_unscaled = torch.cat(original_unscaled_list, dim=0)
        recon_unscaled = torch.cat(recon_unscaled_list, dim=0)
        # lerps_recon_unscaled = torch.cat(lerps_recon_unscaled_list, dim=0) #WIP: Not used!
        print(lerps_for_later)
        lerps_recon_scaled = self.model_block.decoder(lerps_for_later.to(mu)) if self.args.which_model in ["fc", "fc_ae","fc_wass","pca_vae"] else self.model_block.decode(lerps_for_later.to(mu)) #BL3, lerped to reconstruction (scaled coord)
        lerps_recon_unscaled  = unnormalize(lerps_recon_scaled, mean=mean, std=std, min=min, max=max, index=None) #BL3, test_loader latent to reconstruction (raw coord)
        colors_l = torch.LongTensor([20] * lerps_recon_scaled.size(0)).detach().cpu().numpy()
        _, mus_l, logstds_l, _ = self(lerps_recon_scaled)
        mus = torch.cat([mus, mus_l], dim=0)
        logstds = torch.cat([logstds, logstds_l], dim=0)
        colors = np.concatenate([colors, colors_l], axis=0)
        self.plot_manifold_with_colors(self.args, mus.half().detach().cpu().numpy(), logstds.half().detach().cpu().numpy(), None, colors)

        if return_intermediates:
            summary = collections.namedtuple("summary", ["reference", "original", "recon", "lerps", "mus_orig"])
            summary.reference = reference
            summary.original = original_unscaled
            summary.recon = recon_unscaled
            summary.lerps = lerps_recon_unscaled
            summary.mus_orig = mus_orig
            return summary

        print("MDA trajectories are generated...")

        u = mda.Universe(pdb) #universe
        u.load_new(original_unscaled.detach().cpu().numpy()) #overwrite coords
        mda_traj_name = os.path.join(self.args.save_data_directory, self.args.name + "_test.dcd") if self.args.name is not None else os.path.join(self.args.save_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced" + "_test.dcd")
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                w.write(u.atoms)   
    
        u = mda.Universe(pdb) #universe
        u.load_new(recon_unscaled.detach().cpu().numpy()) #overwrite coords
        mda_traj_name = os.path.join(self.args.save_data_directory, self.args.name + "_recon.dcd") if self.args.name is not None else os.path.join(self.args.save_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced" + "_recon.dcd")
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                w.write(u.atoms)   
                
        u = mda.Universe(pdb) #universe
        u.load_new(lerps_recon_unscaled.detach().cpu().numpy()) #overwrite coords
        mda_traj_name = os.path.join(self.args.save_data_directory, self.args.name + "_lerps.dcd") if self.args.name is not None else os.path.join(self.args.save_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced" + "_lerps.dcd")
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                w.write(u.atoms)   

    def generate_molecules_pca(self, test_loader: "test loader original coordinates (BL3)", start_idx: "starting point index, integer", end_idx: "end point index, integer", num_interps: "num of interpolations points", reference: "coord of reference", min=None, max=None, return_intermediates=False) -> "Original && Recon_from_original && Lerp":
        """MOVE to pl.Callback!
        WIP: Add Plotly functions!"""

        original_unscaled_list, recon_unscaled_list, lerps_recon_unscaled_list = [], [], []
        mu_list, logstd_list, color_list, mu_orig_list = [], [], [], []
        len_of_testset = len(test_loader.dataset)

        for i, original in enumerate(test_loader):
            original = original.to(self.device)
            print(original.shape)
            mu = self.model_block(original) #"latend_coordinates of (B,latent_dim)" for PCA B = # frames
            # mu = mu + logstd.exp() * torch.normal(0, 1, size=mu.size()).to(mu) #sampling_noise 0 makes mu as mu...

            ##BELOW: WIP for Interpolations!
            if i == 0: 
                inputs_for_later = mu[start_idx] # dim,
            elif (i == (len(test_loader) - 1)) or (len(test_loader) == 1):
                outputs_for_later = mu[end_idx] # dim,
                if self.args.lerp == "linear":
                    lerps_for_later = self.lerp(start=inputs_for_later, end=outputs_for_later, t=torch.linspace(0, 1, num_interps)[:]) #(Num_interpolations by latent_dim)
                elif self.args.lerp == "geometric":
                    lerps_for_later = self.slerp(start=inputs_for_later, end=outputs_for_later, t=torch.linspace(0, 1, num_interps)[:]) #(Num_interpolations by latent_dim)
            
            inputs = mu[start_idx] # dim,
            outputs = mu[end_idx] # dim,
            if self.args.lerp == "linear":
                lerps = self.lerp(start=inputs, end=outputs, t=torch.linspace(0, 1, num_interps)[1:-1]) #(Num_interpolations by latent_dim)
            elif self.args.lerp == "geometric":
                lerps = self.slerp(start=inputs, end=outputs, t=torch.linspace(0, 1, num_interps)[1:-1]) #(Num_interpolations by latent_dim)

            mean = self.data_mean #make sure dmo is saved as an argparse
            std = self.data_std
            # unnormalize = dl.ProteinDataset.unnormalize if self.args.which_model in ["fc","pca"] else dl.ProteinDatasetDistogram.unnormalize #static method
            
            # reconstruct data (original) --> pcs(mu)--> data' (recon_scaled)
            unnormalize = dl.ProteinDataset.unnormalize
            original_unscaled = unnormalize(original, mean=mean, std=std, min=min, max=max, index=None)
            #recon_scaled = self.model_block.decoder(mu) if self.args.which_model in ["fc","pca"] else self.model_block.decode(mu) #BL3, (scaled coord)
            recon_scaled = self.model_block.inverse_transform(mu)
            recon_unscaled  = unnormalize(recon_scaled, mean=mean, std=std, min=min, max=max, index=None) #BL3, test_loader latent to reconstruction (raw coord)
            
            #lerps_recon_scaled = self.model_block.decoder(lerps.to(mu)) if self.args.which_model in ["fc"] else self.model_block.decode(lerps.to(mu)) #BL3, lerped to reconstruction (scaled coord)
            lerps_recon_scaled = self.model_block.inverse_transform(lerps.to(mu))
            lerps_recon_unscaled  = unnormalize(lerps_recon_scaled, mean=mean, std=std, min=min, max=max, index=None) #BL3, test_loader latent to reconstruction (raw coord)
            colors = torch.cat([torch.LongTensor([i*10] * tensor.size(0)) for i, tensor in enumerate([original, recon_scaled]) ], dim=0).detach().cpu().numpy()
            traj_cats = torch.cat([original, recon_scaled], dim=0) #.detach().cpu().numpy() #BBB,L,3
            #_, mus, logstds, _ = self(traj_cats)
            mus = self.model_block(traj_cats)
            mu_orig_list.append(mus[:original.size(0)])
            mu_list.append(mus)
            #logstd_list.append(logstds)
            color_list.append(colors)

            pdb = os.path.join(self.args.load_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced.pdb") #string
            atom_selection = self.args.atom_selection

            original_unscaled_list.append(original_unscaled)
            recon_unscaled_list.append(recon_unscaled)
            lerps_recon_unscaled_list.append(lerps_recon_unscaled)
        
        mus_orig = torch.cat(mu_orig_list, dim=0) #only for orig mus
        mus = torch.cat(mu_list, dim=0)
        #logstds = torch.cat(logstd_list, dim=0)
        colors = np.concatenate(color_list, axis=0)

        original_unscaled = torch.cat(original_unscaled_list, dim=0)
        recon_unscaled = torch.cat(recon_unscaled_list, dim=0)
        # lerps_recon_unscaled = torch.cat(lerps_recon_unscaled_list, dim=0) #WIP: Not used!
        print(lerps_for_later)
        #lerps_recon_scaled = self.model_block.decoder(lerps_for_later.to(mu)) if self.args.which_model in ["fc"] else self.model_block.decode(lerps_for_later.to(mu)) #BL3, lerped to reconstruction (scaled coord)
        lerps_recon_scaled = self.model_block.inverse_transform(lerps_for_later.to(mu))
        lerps_recon_unscaled  = unnormalize(lerps_recon_scaled, mean=mean, std=std, min=min, max=max, index=None) #BL3, test_loader latent to reconstruction (raw coord)
        colors_l = torch.LongTensor([20] * lerps_recon_scaled.size(0)).detach().cpu().numpy()
        #_, mus_l, logstds_l, _ = self(lerps_recon_scaled)
        mus_l = self.model_block(lerps_recon_scaled)
        mus = torch.cat([mus, mus_l], dim=0)
        #logstds = torch.cat([logstds, logstds_l], dim=0)
        colors = np.concatenate([colors, colors_l], axis=0)
        # self.plot_manifold_with_colors(self.args, mus.detach().cpu().numpy(), logstds.detach().cpu().numpy(), None, colors)

        if return_intermediates:
            summary = collections.namedtuple("summary", ["reference", "original", "recon", "lerps", "mus_orig"])
            summary.reference = reference
            summary.original = original_unscaled
            summary.recon = recon_unscaled
            summary.lerps = lerps_recon_unscaled
            summary.mus_orig = mus_orig
            return summary

        print("MDA trajectories are generated...")

        u = mda.Universe(pdb) #universe
        u.load_new(original_unscaled.detach().cpu().numpy()) #overwrite coords
        mda_traj_name = os.path.join(self.args.save_data_directory, self.args.name + "_test.dcd") if self.args.name is not None else os.path.join(self.args.save_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced" + "_test.dcd")
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                w.write(u.atoms)   
    
        u = mda.Universe(pdb) #universe
        u.load_new(recon_unscaled.detach().cpu().numpy()) #overwrite coords
        mda_traj_name = os.path.join(self.args.save_data_directory, self.args.name + "_recon.dcd") if self.args.name is not None else os.path.join(self.args.save_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced" + "_recon.dcd")
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                w.write(u.atoms)   
                
        u = mda.Universe(pdb) #universe
        u.load_new(lerps_recon_unscaled.detach().cpu().numpy()) #overwrite coords
        mda_traj_name = os.path.join(self.args.save_data_directory, self.args.name + "_lerps.dcd") if self.args.name is not None else os.path.join(self.args.save_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced" + "_lerps.dcd")
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                w.write(u.atoms)  

    def generate_molecules_with_noise(self, test_loader: "test loader original coordinates (BL3)", start_idx: "starting point index, integer", end_idx: "end point index, integer", num_interps: "num of interpolations points", reference: "coord of reference", min=None, max=None, return_intermediates=False) -> "Original && Recon_from_original && Lerp":
        #WIP: this is only for FC model yet!

        original_unscaled_list, recon_unscaled_list, lerps_recon_unscaled_list, lerps_with_noises_list = [], [], [], []
        mu_list, logstd_list, color_list, mu_orig_list = [], [], [], []
        len_of_testset = len(test_loader.dataset)

        for idx, noise in enumerate(self.args.sampling_noise):
            print(cf.on_yellow(f"Sampling noise {noise}"))
            for i, original in enumerate(test_loader):
                original = original.to(self.device)
                z, mu, logstd, x = self(original) #"latend_coordinates of (B,latent_dim)"
                # if idx == 0 and i == 0:
                #     mu_start = mu[0,:]
                # elif idx ==0 and i == (len(test_loader) - 1):
                #     mu_end = mu[-1, :]
                # if idx > 0:
                mu = mu + logstd.exp() * torch.normal(0, noise, size=mu.size()).to(mu) #sampling_noise 0 makes mu as mu...
                #     mu[0, :] = mu_start
                #     mu[-1,:] = mu_end
                
                ##BELOW: WIP for Interpolations!
                ##WARNING: noise should be added here!, after interpolation!
                if i == 0: 
                    inputs_for_later = mu[start_idx] # dim,
                elif i == (len(test_loader) - 1):
                    outputs_for_later = mu[end_idx] # dim,
                    if self.args.lerp == "linear":
                        lerps_for_later = self.lerp(start=inputs_for_later, end=outputs_for_later, t=torch.linspace(0, 1, num_interps)[:]) #(Num_interpolations by latent_dim)
                    elif self.args.lerp == "geometric":
                        lerps_for_later = self.slerp(start=inputs_for_later, end=outputs_for_later, t=torch.linspace(0, 1, num_interps)[:]) #(Num_interpolations by latent_dim)
                    # lerps_for_later = lerps_for_later + torch.normal(0, noise, size=lerps_for_later.size()).to(mu) #sampling_noise 0 makes mu as mu...
                    lerps_with_noises_list.append(lerps_for_later) #List of Lerps at different noises

                if idx == 0:
                    mean = self.data_mean #make sure dmo is saved as an argparse
                    std = self.data_std
                    unnormalize = dl.ProteinDataset.unnormalize if self.args.which_model in ["fc"] else dl.ProteinDatasetDistogram.unnormalize #static method

                    # print(original.size(), mean.size(), std.size())

                    original_unscaled = unnormalize(original, mean=mean, std=std, min=min, max=max, index=None)
                    recon_scaled = self.model_block.decoder(mu) if self.args.which_model in ["fc"] else self.model_block.decode(mu) #BL3, (scaled coord)
                    print(original.shape, recon_scaled.shape, mu.shape, mean.shape, std.shape)

                    recon_unscaled  = unnormalize(recon_scaled, mean=mean, std=std, min=min, max=max, index=None) #BL3, test_loader latent to reconstruction (raw coord)
                    colors = torch.cat([torch.LongTensor([0] * tensor.size(0)) for i, tensor in enumerate([original]) ], dim=0).detach().cpu().numpy()
                    traj_cats = torch.cat([original], dim=0) #.detach().cpu().numpy() #BBB,L,3
                    _, mus, logstds, _ = self(traj_cats)

                    mu_orig_list.append(mus[:original.size(0)])
                    mu_list.append(mus)
                    logstd_list.append(logstds)
                    color_list.append(colors)
                    original_unscaled_list.append(original_unscaled)
                    recon_unscaled_list.append(recon_unscaled)

        #         psf = self.args.psf_file
                pdb = os.path.join(self.args.load_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced.pdb") #string
                atom_selection = self.args.atom_selection
                print("W&B is done...")

            if idx == 0:
                mus_orig = torch.cat(mu_orig_list, dim=0) #only for orig mus
                mus = torch.cat(mu_list, dim=0)
                logstds = torch.cat(logstd_list, dim=0)
                colors = np.concatenate(color_list, axis=0)
                original_unscaled = torch.cat(original_unscaled_list, dim=0)
                recon_unscaled = torch.cat(recon_unscaled_list, dim=0)

        path_html = "plotly_visualization_output.html" #overwrite is ok...
        # table = wandb.Table(columns = ["plotly_figure"])
        # fig = make_subplots(rows=1, cols=2, subplot_titles=('Noise', 'Logprob'))
        #                                         #specs=[[{'type': 'scatter3d'}, {'type': 'scatter3d'}]])
        # # fig0 = go.Figure()
        # fig.add_trace(go.Scatter(x=mus[:0].detach().cpu().numpy(), y=mus[:1].detach().cpu().numpy(), marker=dict(color=colors.reshape(-1,)), mode="markers",                             
        #                     hovertemplate =
        #                     '<i>Name</i>: %{text}',
        #                     text = ["Original"] * colors.shape[0] ,), 1,1
        #                     )
        # # fig1 = go.Figure()
        # fig.add_trace(go.Scatter(x=mus[:0].detach().cpu().numpy(), y=mus[:1].detach().cpu().numpy(), marker=dict(color=colors.reshape(-1,)), mode="markers",                             
        #                     hovertemplate =
        #                     '<i>Name</i>: %{text}',
        #                     text = ["Original"] * colors.shape[0] ,), 1,2
        #                     )

        lerps_with_noises = torch.stack(lerps_with_noises_list, dim=0) #-> (nnoises, B, dim)
        mus_l_list, logstds_l_list, colors_l_list, colors_logprob_list = [], [], [], []
        for idx, (lerps_for_later, noise) in enumerate(zip(lerps_with_noises.unbind(dim=0), self.args.sampling_noise)):
            lerps_recon_scaled = self.model_block.decoder(lerps_for_later.to(mu)) if self.args.which_model in ["fc"] else self.model_block.decode(lerps_for_later.to(mu)) #BL3, lerped to reconstruction (scaled coord)
            lerps_recon_unscaled  = unnormalize(lerps_recon_scaled, mean=mean, std=std, min=min, max=max, index=None) #BL3, test_loader latent to reconstruction (raw coord)
            colors_l = torch.LongTensor([noise*1e3] * lerps_recon_scaled.size(0)).detach().cpu().numpy() #highest is value 100; 256 is reserved for original
            _, mus_l, logstds_l, _ = self(lerps_recon_scaled)
            color_logprob = torch.distributions.Normal(0, 1).log_prob(mus_l).sum().item()

            mus_l_list.append(mus_l)
            logstds_l_list.append(logstds_l)
            colors_l_list.append(colors_l)
            colors_logprob_list.append(np.array([color_logprob] * mus_l.size(0)))

            # fig.add_trace(go.Scatter(x=mus_l[:0].detach().cpu().numpy(), y=mus_l[:1].detach().cpu().numpy(),  marker=dict(color=10*(idx+1)), mode="lines+markers",                             
            #                 hovertemplate =
            #                 '<i>Name</i>: %{text}',
            #                 text = colors_l ,), 1, 1
            #                 )
            # fig.add_trace(go.Scatter(x=mus_l[:0].detach().cpu().numpy(), y=mus_l[:1].detach().cpu().numpy(), marker=dict(color=10*(idx+1)), mode="lines+markers",                             
            #                 hovertemplate =
            #                 '<i>Name</i>: %{text}',
            #                 text = colors_logprob_list[-1] ,), 1,2
            #                 )

            u = mda.Universe(pdb) #universe
            u.load_new(lerps_recon_unscaled.detach().cpu().numpy()) #overwrite coords
            mda_traj_name = os.path.join(self.args.save_data_directory, self.args.name + f"_lerps_{noise}.dcd") if self.args.name is not None else os.path.join(self.args.save_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced" + f"_lerps_{noise}.dcd")
            with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
                for ts in u.trajectory:
                    w.write(u.atoms)   

        # fig0.write_html(path_to_plotly_html, auto_play = False)
        # table.add_data(wandb.Html( open(path_to_plotly_html) ))
        # fig.write_html(path_html, auto_play = False)
        # table.add_data(wandb.Html( open(path_to_plotly_html) ))
        # wandb.log({f"Latent Plot with Interps": wandb.Html( open(path_html) )})

        mus_l = torch.cat(mus_l_list, dim=0)
        logstds_l = torch.cat(logstds_l_list, dim=0)
        colors_l = np.concatenate(colors_l_list, axis=0)
        colors_logprob_l = np.concatenate(colors_logprob_list, axis=0)
        # from sklearn.preprocessing import MinMaxScaler
        # scaler = MinMaxScaler(feature_range=(10, 256), copy=True, clip=False) #; 10 to 256 range; 0 is for original
        # colors_logprob_l = scaler.fit_transform(colors_logprob_l.reshape(-1,1)) #scaled
        colors_logprob_l = np.concatenate([np.zeros(mus.size(0)), colors_logprob_l.reshape(-1,)], axis=0)

        mus = torch.cat([mus, mus_l], dim=0)
        logstds = torch.cat([logstds, logstds_l], dim=0)
        colors = np.concatenate([colors, colors_l], axis=0)
        self.plot_manifold_with_colors(self.args, mus.detach().cpu().numpy(), logstds.detach().cpu().numpy(), None, colors, get_logpdf=True, logpdf_colors=colors_logprob_l)



    def analyze_molecule(self, test_loader: "test loader original coordinates (BL3)", start_idx: "starting point index, integer", end_idx: "end point index, integer", num_interps: "num of interpolations points", reference: "coord of reference", min=None, max=None):
        if self.args.which_model == "pca":
            summary = self.generate_molecules_pca(test_loader=test_loader, start_idx=start_idx, end_idx=end_idx, num_interps=num_interps, reference=reference, min=min, max=max, return_intermediates=True) #return namedtuple
        else: 
            summary = self.generate_molecules(test_loader=test_loader, start_idx=start_idx, end_idx=end_idx, num_interps=num_interps, reference=reference, min=min, max=max, return_intermediates=True) #return namedtuple
        reference = summary.reference #.detach().cpu().numpy() 
        original_unscaled = summary.original #.detach().cpu().numpy() 
        recon_unscaled = summary.recon #.detach().cpu().numpy()  
        lerps_recon_unscaled = summary.lerps #.detach().cpu().numpy()  
        mus_orig = summary.mus_orig

        sizes = original_unscaled.size() #for expansion

        fig, ax = plt.subplots(3, 3, figsize=(10,10), sharex='col')
        points = []

        print("Original vs Reconstruction")
        rmsds = rmsd_torch(original_unscaled.permute(0,2,1).contiguous(), recon_unscaled.permute(0,2,1).contiguous()) #->(B,)
        gdts = gdt_torch(original_unscaled.permute(0,2,1).contiguous(), recon_unscaled.permute(0,2,1).contiguous(), torch.arange(0.5, 10.5, 0.5).to(original_unscaled))
        tms = tmscore_torch(original_unscaled.permute(0,2,1).contiguous(), recon_unscaled.permute(0,2,1).contiguous())
        points += [rmsds, gdts, tms]

        print("Reference vs Original")
        rmsds = rmsd_torch(reference.expand(sizes).permute(0,2,1).contiguous(), original_unscaled.permute(0,2,1).contiguous()) #->(B,)
        gdts = gdt_torch(reference.expand(sizes).permute(0,2,1).contiguous(), original_unscaled.permute(0,2,1).contiguous(), torch.arange(0.5, 10.5, 0.5).to(original_unscaled))
        tms = tmscore_torch(reference.expand(sizes).permute(0,2,1).contiguous(), original_unscaled.permute(0,2,1).contiguous()) 
        points += [rmsds, gdts, tms]

        print("Reference vs Reconstruction")
        rmsds = rmsd_torch(reference.expand(sizes).permute(0,2,1).contiguous(), recon_unscaled.permute(0,2,1).contiguous()) #->(B,)
        gdts = gdt_torch(reference.expand(sizes).permute(0,2,1).contiguous(), recon_unscaled.permute(0,2,1).contiguous(), torch.arange(0.5, 10.5, 0.5).to(original_unscaled))
        tms = tmscore_torch(reference.expand(sizes).permute(0,2,1).contiguous(), recon_unscaled.permute(0,2,1).contiguous())        
        points += [rmsds, gdts, tms]

        points = list(map(lambda inp: inp.detach().cpu().numpy().reshape(-1, ), points)) #numpy lists
        point_dict = collections.defaultdict(list)
        np.savez(os.path.join(self.args.save_data_directory, f"bar_{self.args.name}.npz"), points=points)


        for i, (p, n, t) in enumerate(zip(points, ["rmsd","gdt","tm"]*3, np.repeat(np.array(["Original vs Reconstruction", "Reference vs Original", "Reference vs Reconstruction"]), 3, axis=0) )):
            ax.flatten()[i].hist(p, bins=15, density=True)
            if i >= 6:
                ax.flatten()[i].set_xlabel(n)
            if i % 3 == 0:
                ax.flatten()[i].set_ylabel("Density")            
            if (i-1) % 3 == 0 :
                ax.flatten()[i].set_title(t)
            if i % 3 == 0:
                point_dict["rmsd_mean"].append(p.mean())  
                point_dict["rmsd_std"].append(p.std())  
            if (i - 1) % 3 == 0:
                point_dict["gdt_mean"].append(p.mean())  
                point_dict["gdt_std"].append(p.std())  
            if (i - 2) % 3 == 0:
                point_dict["tm_mean"].append(p.mean())  
                point_dict["tm_std"].append(p.std())  
        fig.savefig(os.path.join(self.args.save_data_directory, "analyze_saved_bar.png"))

        retained_dssp = self.calc_dssp
        point_dict["dssp_percentage"].extend([retained_dssp.mean()*100, np.nan, np.nan])

        fig.tight_layout()
        buf = io.BytesIO()
        fig.savefig(buf)
        buf.seek(0)
        img = PIL.Image.open(buf)
        if not self.args.nolog:
            wimg = wandb.Image(img)
            wandb.log({"Plots": wimg})
        print("Saving histograms...")

        df = pd.DataFrame.from_dict(point_dict)
        if not self.args.nolog:
            table = wandb.Table(dataframe=df)
            wandb.log({"Stats": table})
        print("Saving statistics...")

        if self.args.which_model not in ['fc']:
            NBINS = 40 #LINSPACING
            PERCENTAGE = 0.3
            RELPERCT = (1+PERCENTAGE) #130 % coverage
            RELPERCT_SAMESIGN = (1-PERCENTAGE) #70 % coverage
            minx, maxx, miny, maxy = mus_orig[:, 0].min().item(), mus_orig[:, 0].max().item(), mus_orig[:, 1].min().item(), mus_orig[:, 1].max().item()
            SIGNX = (np.sign(minx) * np.sign(maxx))
            SIGNY = (np.sign(miny) * np.sign(maxy))

            if SIGNX == 1:
                if np.sign(minx) == -1 and np.sign(maxx) == -1:
                    relminx, relmaxx = RELPERCT*minx, RELPERCT_SAMESIGN*maxx
                else:
                    relminx, relmaxx = RELPERCT_SAMESIGN*minx, RELPERCT*maxx
            else:
                relminx, relmaxx = list(map(lambda inp: RELPERCT*inp, [minx, maxx]))
            if SIGNY == 1:
                if np.sign(minx) == -1 and np.sign(maxx) == -1:
                    relminy, relmaxy = RELPERCT*miny, RELPERCT_SAMESIGN*maxy
                else:
                    relminy, relmaxy = RELPERCT_SAMESIGN*miny, RELPERCT*maxy
            else:
                relminy, relmaxy = list(map(lambda inp: RELPERCT*inp, [miny, maxy]))
            # if (np.sign(minx) == np.sign(maxx)) or (np.sign(miny) == np.sign(maxy)):
            #     if np.sign(minx) == -1 and np.sign(maxx) == -1:
            #         relminx, relmaxx = RELPERCT*minx, RELPERCT_SAMESIGN*maxx
            #     elif np.sign(minx) == 1 and np.sign(maxx) == 1:
            #         relminx, relmaxx = RELPERCT_SAMESIGN*minx, RELPERCT*maxx
            #     elif np.sign(miny) == -1 and np.sign(maxy) == -1:
            #         relminy, relmaxy = RELPERCT*miny, RELPERCT_SAMESIGN*maxy
            #     elif np.sign(miny) == 1 and np.sign(maxy) == 1:
            #         relminy, relmaxy = RELPERCT_SAMESIGN*miny, RELPERCT*maxy
            # else:        
            #     relminx, relmaxx, relminy, relmaxy = list(map(lambda inp: RELPERCT*inp, [minx, maxx, miny, maxy]))

            
            
            mesh_inputs = torch.stack([
                                        torch.arange(relminx, relmaxx, (relmaxx - relminx)/NBINS),
                                        torch.arange(relminy, relmaxy, (relmaxy - relminy)/NBINS)
                                        ], dim=0) #-> (2, NBINS)
        else:
            #For vae (which_model = fc), this works!
            mesh_inputs = torch.stack([
                                        torch.linspace(-0.2, 0.2, 40),
                                        torch.linspace(-0.2, 0.2, 40)
                                        ], dim=0) #-> (2, NBINS)

        X = list(map(lambda inp: inp.contiguous().view(-1, ), torch.meshgrid(*mesh_inputs.unbind(dim=0)) )) #-> List: 2 elements of size (dim x dim x ... dim; 2 times) -> 64 x dim^2
        X = torch.stack(X, dim=1) #-> (npoints, 2)
        # X = torch.cat([X, X.new_zeros(X.size(0), 62)], dim=1) #->(npoints, 64);; lat_dim
        # pl.seed_everything(42)
        n_components = self.model_block.n_components_ if self.args.which_model == "pca" else self.model_block.decoder.hidden_dims[0]
        X = torch.cat([X, torch.normal(0, 0.005, size=(X.size(0), n_components-2)).to(X)], dim=1) #->(npoints, n_components);; lat_dim
        npoints = mesh_inputs.size(1)
        if self.args.which_model == "pca":
            mesh_recons_scaled = self.model_block.inverse_transform(X)
        else: 
            mesh_recons_scaled = self.model_block.decoder(X) #-> npoints, 
        mean = self.data_mean #make sure dmo is saved as an argparse
        std = self.data_std
        unnormalize = dl.ProteinDataset.unnormalize if self.args.which_model in ["fc","pca","fc_ae","fc_wass","pca_vae"] else dl.ProteinDatasetDistogram.unnormalize #static method
        mesh_recons_unscaled = unnormalize(mesh_recons_scaled, mean=mean, std=std, min=min, max=max, index=None) #->(npoints, L, 3)
        sizes = mesh_recons_unscaled.size()

        ##Cell Press: Coupling Molecular Dynamics and Deep Learning to Mine Protein Conformational Space##
        ##Deformation scores (stretch + compress)
        pair_dists = torch.cdist(mesh_recons_unscaled, mesh_recons_unscaled) #->(npoints,L,L)
        b, l, l = pair_dists.size()
        flat_pair_dists_max = pair_dists.amax(dim=-1).view(-1, ).type(torch.float) #->npoints,L; max dists
        flat_pair_dists_min = pair_dists.amin(dim=-1).view(-1, ).type(torch.float) #->npoints,L; max dists
        stretch = torch.where(flat_pair_dists_max < 2., torch.tensor(0.), flat_pair_dists_max - torch.tensor(2.)).view(b, l).sum(dim=-1)
        compress = torch.where(flat_pair_dists_min > 0.1, torch.tensor(0.), torch.tensor(5.) - flat_pair_dists_min*50.).view(b, l).sum(dim=-1)

        probs = torch.distributions.Normal(0, 1).log_prob(X).sum(dim=-1) #1. ->(npoints); logprob
        rmsds = rmsd_torch(reference.expand(sizes).permute(0,2,1).contiguous(), mesh_recons_unscaled.permute(0,2,1).contiguous()) #2. ->(npoints,)
        gdts = gdt_torch(reference.expand(sizes).permute(0,2,1).contiguous(), mesh_recons_unscaled.permute(0,2,1).contiguous(), torch.arange(0.5, 10.5, 0.5).to(original_unscaled)) #3. ->(npoints,)
        tms = tmscore_torch(reference.expand(sizes).permute(0,2,1).contiguous(), mesh_recons_unscaled.permute(0,2,1).contiguous()) #4. ->(npoints,)
        deformation = stretch + compress #5. ->(npoints,)

        from plotly.subplots import make_subplots
        import plotly.graph_objs as go
        fig = make_subplots(rows=3, cols=2, subplot_titles=('Log Prob','RMSD','GDT','TM', "Deformation"))
        fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=probs.detach().cpu().numpy().reshape(npoints, npoints), showscale=False,
                                    contours=dict(
                                    coloring ='heatmap',
                                    showlabels = True, # show labels on contours
                                    labelfont = dict( # label font properties
                                        size = 12,
                                        color = 'white',
                                    ))
                                    ), 1, 1)      
        fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 1, 1)
        fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=rmsds.detach().cpu().numpy().reshape(npoints, npoints), showscale=False, 
                                    contours=dict(
                                    coloring ='heatmap',
                                    showlabels = True, # show labels on contours
                                    labelfont = dict( # label font properties
                                        size = 12,
                                        color = 'white',
                                    ))
                                    ), 1, 2)    
        fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 1, 2)
        fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=gdts.detach().cpu().numpy().reshape(npoints, npoints), showscale=False,
                                    contours=dict(
                                    coloring ='heatmap',
                                    showlabels = True, # show labels on contours
                                    labelfont = dict( # label font properties
                                        size = 12,
                                        color = 'white',
                                    ))
                                    ), 2, 1)    
        fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 2, 1)
        fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=tms.detach().cpu().numpy().reshape(npoints, npoints), showscale=False,
                                    contours=dict(
                                    coloring ='heatmap',
                                    showlabels = True, # show labels on contours
                                    labelfont = dict( # label font properties
                                        size = 12,
                                        color = 'white',
                                    ))
                                    ), 2, 2)    
        fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 2, 2)
        fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=deformation.detach().cpu().numpy().reshape(npoints, npoints), showscale=False,
                                    contours=dict(
                                    coloring ='heatmap',
                                    showlabels = True, # show labels on contours
                                    labelfont = dict( # label font properties
                                        size = 12,
                                        color = 'white',
                                    ))
                                    ), 3, 1)    
        fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 3, 1)
        fig.update_layout(autosize=True)
        
        path_to_plotly_html = os.path.join(self.args.save_data_directory, "plotly_figure.html")
        fig.write_html(path_to_plotly_html, auto_play = False)
        if not self.args.nolog:
            wandb.log({"html": wandb.Html(open(path_to_plotly_html))})
        # fig.write_image(os.path.splitext(path_to_plotly_html)[0] + "_subplots.png")
        print("Saving countours...")
        np.savez(os.path.join(self.args.save_data_directory, f"scatter_{self.args.name}.npz"), mesh=mesh_inputs.detach().cpu().numpy(), mus=mus_orig.detach().cpu().numpy(), prob = probs.detach().cpu().numpy(), rmsd = rmsds.detach().cpu().numpy(), gdt = gdts.detach().cpu().numpy(), tm= tms.detach().cpu().numpy(),  deform = deformation.detach().cpu().numpy())
        print("Saving npz...")

    @property
    def calc_dssp(self, ):
        print("Computing DSSP...")
        import mdtraj as md
        dssp_original_trajs = os.path.join(self.args.save_data_directory, self.args.dssp_original_trajs[0])
        dssp_recon_trajs = os.path.join(self.args.save_data_directory, self.args.dssp_recon_trajs[0])
        pdb = os.path.join(self.args.load_data_directory, os.path.splitext(self.args.pdb_file)[0] + "_reduced.pdb") #string
        traj_orig = md.load(dssp_original_trajs, top=pdb)
        traj_recon = md.load(dssp_recon_trajs, top=pdb)
        dssp_orig = md.compute_dssp(traj_orig, simplified=True) #(nframes, nres)
        dssp_recon = md.compute_dssp(traj_recon, simplified=True) #(nframes, nres)
        np.savez(os.path.join(self.args.save_data_directory, "analyze_saved_dssp.npz"), dssp_orig = dssp_orig, dssp_recon = dssp_recon)
        retained_dssp = (dssp_orig == dssp_recon).mean(axis=-1) #->(nframes)
        return retained_dssp


if __name__ == '__main__':
    kwargs.update(which_emodel="physnet", ani_block=dict(), schnet_block=dict(), physnet_block=dict(), spectral_norm=False)
    #total_dataset = dl.multiprocessing_dataset()
    #dataset = dl.TorchDataset(total_dataset, 'z15')
    model = Model(**kwargs)

    #dataloaded = dl.DataLoader(dataset, 128).dataloader
    dataloaded = dl.DataLoader(batch_size=100, protein_name="pentapeptide").dataloader
    coords, species = next(iter(dataloaded))
    #b, l, c = coords.size()
    #species = torch.LongTensor(torch.tensor([[6] * l]).repeat(b, 1))
    #print(species, next(iter(dataloaded)))
    #print(model.pre_block([species, next(iter(dataloaded))]) )
    print(model([species, coords]), species.shape)

