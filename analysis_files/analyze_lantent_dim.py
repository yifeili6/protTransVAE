import numpy as np
import matplotlib.pyplot as plt
#from sklearn.preprocessing import LabelEncoder
import collections
import argparse
import os
import io, PIL
import torch
import ray
#from google.colab import files
#%pip install -U kaleido
data1        = np.load('scatter_SerT_IFOF_vae.npz')
data2        = np.load('scatter_SerT_OC_vae.npz')


which_model = 'fc'
# n1 = 0 
# n2 = 15
for i in range(41,63):
  for j in range(i):
    data        = data1

    # line 1026

    #mesh_inputs = data['mesh']
    mus_orig    = data['mus']
    # mus_orig    = mus_orig[0:-1:1000]
    probs       = data['prob']
    rmsds       = data['rmsd']
    gdts        = data['gdt']
    tms         = data['tm']
    deformation = data['deform']




    relminx, relmaxx, relminy, relmaxy = -0.4, 0.28, -0.52, 0.28
    mesh_inputs = torch.stack([
                                torch.linspace(relminx, relmaxx, 40),
                                torch.linspace(relminy, relmaxy, 40)
                                ], dim=0) #-> (2, NBINS)



    npoints = mesh_inputs.size(1)


    n1 = i 
    n2 = j

    nbinsx_=40 
    nbinsy_=40
    ### 2D latent space plot
    from plotly.subplots import make_subplots
    import plotly.graph_objs as go

    fig = make_subplots(rows=2, cols=2, subplot_titles=('IFOF','OC'),
                        horizontal_spacing = 0.1, vertical_spacing=0.1)

    fig.add_trace(go.Contour(x=mesh_inputs[0], y=mesh_inputs[1], z=probs.reshape(npoints, npoints), showscale=False,
                                contours=dict(
                                coloring ='heatmap',
                                showlabels = True, # show labels on contours
                                labelfont = dict( # label font properties
                                    size = 12,
                                    color = 'white',
                                ))
                                ), 1, 1)      

    fig.add_trace(go.Scatter(x=mus_orig[:,n1], y=mus_orig[:,n2], mode="markers"), 1, 1)

    fig.add_trace(go.Histogram2d(x=mus_orig[:,n1], y=mus_orig[:,n2], nbinsx=nbinsx_, nbinsy=nbinsy_), 2, 1)

    data        = data2


    mus_orig    = data['mus']
    probs       = data['prob']
    rmsds       = data['rmsd']
    gdts        = data['gdt']
    tms         = data['tm']
    deformation = data['deform']

    fig.add_trace(go.Contour(x=mesh_inputs[0], y=mesh_inputs[1], z=probs.reshape(npoints, npoints), showscale=False,
                                contours=dict(
                                coloring ='heatmap',
                                showlabels = True, # show labels on contours
                                labelfont = dict( # label font properties
                                    size = 12,
                                    color = 'white',
                                ))
                                ), 1, 2)      
    fig.add_trace(go.Scatter(x=mus_orig[:,n1], y=mus_orig[:,n2], mode="markers"), 1, 2)

    fig.add_trace(go.Histogram2d(x=mus_orig[:,n1], y=mus_orig[:,n2], nbinsx=nbinsx_, nbinsy=nbinsy_), 2, 2)


    fig.write_image(f"figsc_{n1}_{n2}.png")
    print(f"{n1}_{n2}")
    #!zip -r log1.zip ./figsc*
