# vaegan_md

To train, <br>
<code> python -m main_yifei --psf_file 3f48final.psf --pdb_file 3f48finaleqnbfix.pdb --trajectory_files force5tm18_afterreleaseeq_nbfix.dcd --strategy none --batch_size 256 --scheduler cosine -gaccm 1 --atom_selection "protein and backbone" --train_mode train --num_epochs 5000 --lr 0.005</code>

To resume training, <br>
<code> python -m main_yifei --psf_file 3f48final.psf --pdb_file 3f48finaleqnbfix.pdb --trajectory_files force5tm18_afterreleaseeq_nbfix.dcd --strategy none --batch_size 256 --scheduler cosine -gaccm 1 --atom_selection "protein and backbone" --train_mode train --num_epochs 5000 --lr 0.005 -ckpt [filename.ckpt] </code>

To test, <br>
<code> python -m main_yifei --psf_file 3f48final.psf --pdb_file 3f48finaleqnbfix.pdb --trajectory_files force5tm18_afterreleaseeq_nbfix.dcd --strategy none --batch_size 256 --scheduler cosine -gaccm 1 --atom_selection "protein and backbone" --train_mode test --num_epochs 5000 --lr 0.005 -ckpt [filename.ckpt]</code>

To make original, reconstructed and interpolated trajectories (DCD format), <br>
<code> python -m main_yifei --psf_file 3f48final.psf --pdb_file 3f48finaleqnbfix.pdb --trajectory_files force5tm18_afterreleaseeq_nbfix.dcd --strategy none --batch_size 256 --scheduler cosine -gaccm 1 --atom_selection "protein and backbone" --train_mode sample --num_epochs 5000 --lr 0.005 -ckpt [filename.ckpt]</code>

To train without logging, <br>
<code> CUDA_VISIBLE_DEVICES=0 python -m main --psf_file 3f48final.psf --pdb_file 3f48finaleqnbfix.pdb --trajectory_files force5TM18_water_nbfix_2.dcd --align_selection "backbone and resid 20-500" --atom_selection "backbone and resid 20-500" --strategy none --ngpus -1 --batch_size 512 --scheduler reduce -gaccm 1 --train_mode train --num_epochs 15000 --lr 0.001 --which_model fc --split_portion 40 60 --split_method middle --nolog --load_model_directory [where to save deeplearning model] --load_data_directory [where is your psf/pdb/dcd?] --save_data_directory [where to save interpolated data?] --standard_file [npy file to save mean/std of aligned trajectory]
</code>
