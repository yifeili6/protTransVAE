import torch, torchani
import transformers as tfm
import dataloader as dl 
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mdtraj
import MDAnalysis as mda
from MDAnalysis.analysis.base import AnalysisBase
from MDAnalysis.core.universe import Universe
from MDAnalysis.core.groups import AtomGroup
import tqdm
import pickle, json, os, copy
import gc
import collections
import networkx as nx
import yaml
import logging
import warnings
import wandb
import argparse
from typing import *
from curtsies import fmtfuncs as cf
import io, PIL
from plotly.subplots import make_subplots
import plotly.graph_objs as go
from dataloader import alignTrajectory
from MDAnalysis.analysis.distances import dist as mdadist
from joblib import Parallel, delayed
import multiprocessing
from MDAnalysis.analysis.align import _fit_to
# from MDAnalysis.transformations.fit import fit_rot_trans
from MDAnalysis.lib.log import ProgressBar
from MDAnalysis.transformations.base import TransformationBase
from MDAnalysis.analysis import align
from MDAnalysis.lib.transformations import euler_from_matrix, euler_matrix
from MDAnalysis.coordinates.base import Timestep, ProtoReader
import logging
import ast
 

AVERAGE_DIMS_MDTRAJ = np.array([ 9.738272,  9.738272, 10.814863,  90.     ,  90.     ,  90.     ]) #nanometer unit

# https://github.com/p-j-smith/lipyphilic/tree/master/src/lipyphilic #for lipids
# https://docs.mdanalysis.org/2.3.0/_modules/MDAnalysis/analysis/bat.html#BAT
def preprocess_trajectories(args: argparse.ArgumentParser):
    # assert isinstance(atom_pairs, list) and isinstance(atom_pairs[0], (tuple, list))
    ab = args.analysis_backend

    if ab == "mdtraj": #Nanometers
        print(cf.on_red("USE for SASA only!"))
        trajs = []
        prot_ref = mdtraj.load(os.path.join(args.load_data_directory, args.pdb_file)) #reduced_PBD!
        # METHOD 1 for multiple trajs #https://github.com/mdtraj/mdtraj/blob/1a757274f6f9acd6c9c847c72f5857e9c1046d17/mdtraj/core/trajectory.py#L781:~:text=def%20join(self%2C%20other%2C%20check_topology%3DTrue%2C%20discard_overlapping_frames%3DFalse)%3A
        for one_traj in args.trajectory_files:
            traj = mdtraj.load(os.path.join(args.load_data_directory, one_traj), top=os.path.join(args.load_data_directory, args.pdb_file))
            trajs.append(traj)
        trajs: mdtraj.Trajectory = mdtraj.join(trajs) #concatenate 
        #METHOD 2 (OR simply load as)
        #mdtraj.load(args.trajectory_files) #List of trajectories! #https://github.com/mdtraj/mdtraj/blob/1a757274f6f9acd6c9c847c72f5857e9c1046d17/mdtraj/core/trajectory.py#L781:~:text=def%20load(filename_or_filenames%2C%20discard_overlapping_frames%3DFalse%2C%20**kwargs)%3A

        trajs.superpose(prot_ref) #inplace
        indices = trajs.topology.select(args.atom_sasa_indices)
        trajs = trajs.atom_slice(indices) #atom_selection Trajectory
        prot_traj, prot_ref, atom_selection = trajs, prot_ref, args.atom_selection #In nm unit
        boxdims_broadcast = np.broadcast_to(AVERAGE_DIMS_MDTRAJ, (trajs.n_frames, 6))
        (unitcell_lengths, unitcell_angles) = (prot_traj.unitcell_lengths, prot_traj.unitcell_angles) if not isinstance(prot_traj.unitcell_angles, type(None)) else (boxdims_broadcast[:, :3], boxdims_broadcast[:, 3:])
        box_dims = boxdims_broadcast.copy() #(nframes, 6) only from Trajectory files

        #METHOD 1 for creating a new class
        prot_traj = prot_traj.__class__(xyz=prot_traj.xyz, topology=prot_traj.topology, unitcell_angles=unitcell_angles,
                              unitcell_lengths=unitcell_lengths, time=prot_traj.time) #CLASSMETHOD: https://github.com/mdtraj/mdtraj/blob/1a757274f6f9acd6c9c847c72f5857e9c1046d17/mdtraj/core/trajectory.py#L299:~:text=newtraj%20%3D-,self.__class__(,unitcell_angles%3Dunitcell_angles),-if%20rmsd_traces%20is
        #METHOD 2 (OR simply generate as);; inplace
        # prot_traj.unitcell_lengths(unitcell_lengths); prot_traj.unitcell_angles(unitcell_angles) #https://github.com/mdtraj/mdtraj/blob/1a757274f6f9acd6c9c847c72f5857e9c1046d17/mdtraj/core/trajectory.py#L781:~:text=self._unitcell_angles-,%40unitcell_lengths.setter,warn_on_cast%3DFalse%2C%20add_newaxis_on_deficient_ndim%3DTrue),-%40property
        assert (not isinstance(prot_traj.unitcell_angles, type(None))) and (not isinstance(prot_traj.unitcell_lengths, type(None))), "trajectory's boxdims exist now!"
    elif ab == "mdanalysis": #Angstrom
        prot_traj, prot_ref, atom_selection = alignTrajectory(args) #In Angstrom unit; prot_traj is UNIVERSE; BOX dim is given here!
        box_dims = np.array([ts.dimensions for ts in prot_traj.trajectory]) #(nframes, 6) only from Trajectory files
    
    return prot_traj, prot_ref, atom_selection, box_dims, args #Input to compute_distances method!

def compute_distances(atom_pairs: List[tuple], *input_args):
    """
    NOTES:
    MDAnalysis can be more useful between different Trajectories/Universes 
    """
    prot_traj, prot_ref, atom_selection, box_dims, args = input_args
    ab = args.analysis_backend
    atom_pairs = np.array(atom_pairs)

    if ab == "mdtraj":
        assert atom_pairs.ndim == 2 and atom_pairs.shape[1] == 2 and isinstance(prot_traj, mdtraj.Trajectory), "shape is wrong"
        dists = mdtraj.compute_distances(prot_traj, atom_paris) #(nframes, npairs)
    elif ab == "mdanalysis":
        assert atom_pairs.ndim == 2 and atom_pairs.shape[1] == 2 and isinstance(prot_traj, mda.Universe), "shape is wrong"
        distcalc = DistanceCalculations(atomgroup=prot_traj.atoms, atom_pairs=atom_pairs) #initialize everything here!
        dists = distcalc.run().results.distances #Use Custom AnalysisBase!!!! (nframes, npairs)

        # num_cores = multiprocessing.cpu_count()
        # def distcalc_func():
        #     distcalc.run()
        #     return distcalc.results
    
    return dists

class DistanceCalculations(AnalysisBase):  # subclass AnalysisBase
    def __init__(self, atomgroup: Union[AtomGroup, Universe], atom_pairs: List[tuple], verbose=True, args=None):
        """
        Set up the initial analysis parameters.
        """
        # must first run AnalysisBase.__init__ and pass the trajectory
        assert isinstance(atomgroup, AtomGroup), "input must be an Universe or AtomGroup instance..."
        trajectory = atomgroup.universe.trajectory
        super(DistanceCalculations, self).__init__(trajectory,
                                               verbose=verbose)
        # set atomgroup as a property for access in other methods

        self.atomgroup = atomgroup
        atom_pairs: Union[List[str], List[Tuple[int]]] = list(map(lambda inp: ast.literal_eval(inp) if isinstance(inp, str) else inp, atom_pairs )) #to -> List[int]
        self.atom_pairs = np.array(atom_pairs) #(pairs, 2)
        # we can calculate masses now because they do not depend
        # on the trajectory frame.
        # self.masses = self.atomgroup.masses
        # self.total_mass = np.sum(self.masses)

    def _prepare(self):
        """
        Create array of zeroes as a placeholder for results.
        This is run before we begin looping over the trajectory.
        """
        # This must go here, instead of __init__, because
        # it depends on the number of frames specified in run().
        self.results.distances = np.zeros((self.n_frames, self.atom_pairs.shape[0]))
        self.results.displacement = np.zeros((self.n_frames, self.atom_pairs.shape[0], 3 ))

        # We put in 6 columns: 1 for the frame index,
        # 1 for the time, 4 for the radii of gyration

    def _single_frame(self):
        """
        This function is called for every frame that we choose
        in run().
        """
        row, col = self.atom_pairs[:,0], self.atom_pairs[:,1]
        row_ = " ".join(row.astype(str).tolist())
        col_ = " ".join(col.astype(str).tolist())
        # _, _, dists = mdadist(self.atomgroup.universe.select_atoms(f"index {row_}"), self.atomgroup.universe.select_atoms(f"index {col_}"))
        dists_collected = np.array([mdadist(self.atomgroup.universe.select_atoms(f"index {r}"), self.atomgroup.universe.select_atoms(f"index {c}"))[2] for r, c in zip(row, col)])
        self.results.distances[self._frame_index, :] = dists_collected.reshape(-1, )

        ag0_pos_r, ag0_pos_c = np.array([self.atomgroup.universe.select_atoms(f"index {r}").positions for r in row]), np.array([self.atomgroup.universe.select_atoms(f"index {c}").positions for c in col])
        ag0_displacement = np.squeeze(ag0_pos_r - ag0_pos_c) #(npairs, 3)
        self.results.displacement[self._frame_index, :, :] = ag0_displacement

    def _conclude(self):
        """
        Finish up by calculating an average and transforming our
        results into a DataFrame.
        """
        # by now self.result is fully populated
        print("Distance calculation is done!")


class fit_rot_trans(TransformationBase):
    """
    Parameters
    ----------
    ag : Universe or AtomGroup
       structure to translate and rotate, a
       :class:`~MDAnalysis.core.groups.AtomGroup` or a whole
       :class:`~MDAnalysis.core.universe.Universe`
    reference : Universe or AtomGroup
       reference structure, a :class:`~MDAnalysis.core.groups.AtomGroup` or a whole
       :class:`~MDAnalysis.core.universe.Universe`
    plane: str, optional
        used to define the plane on which the rotations and translations will be removed.
        Defined as a string of the plane. Supported planes are "yz", "xz" and "xy" planes.
    weights : {"mass", ``None``} or array_like, optional
       choose weights. With ``"mass"`` uses masses as weights; with ``None``
       weigh each atom equally. If a float array of the same length as
       `ag` is provided, use each element of the `array_like` as a
       weight for the corresponding atom in `ag`.

    Returns
    -------
    MDAnalysis.coordinates.base.Timestep
    """
    def __init__(self, ag0, ag1, plane=None, weights=None,
                 max_threads=1, parallelizable=True):
        super().__init__(max_threads=max_threads,
                         parallelizable=parallelizable)

        self.ag0 = ag0
        self.ag1 = ag1
        self.plane = plane
        self.weights = weights

        if self.plane is not None:
            axes = {'yz': 0, 'xz': 1, 'xy': 2}
            try:
                self.plane = axes[self.plane]
            except (TypeError, KeyError):
                raise ValueError(f'{self.plane} is not a valid plane') \
                                 from None
        try:
            if self.ag0.atoms.n_residues != self.ag1.atoms.n_residues:
                errmsg = (
                    f"{self.ag0} and {self.ag1} have mismatched "
                    f"number of residues"
                )
                raise ValueError(errmsg)
        except AttributeError:
            errmsg = (
                      f"{self.ag} or {self.reference} is not valid "
                      f"Universe/AtomGroup"
            )
            raise AttributeError(errmsg) from None

        self.ag0, self.ag1 = align.get_matching_atoms(self.ag0.atoms,
                                                         self.ag1.atoms) #atom groups of selected
        self.weights = align.get_weights(self.ag0.atoms, weights=self.weights)
        self.ag0_com = self.ag0.center(self.weights)
        self.ag1_com = self.ag1.center(self.weights)

        self.ag0_coordinates = self.ag0.atoms.positions - self.ag0_com
        self.ag1_coordinates = self.ag1.atoms.positions - self.ag1_com

    def _transform(self, ts: ProtoReader):
        frame = ts.frame #ag0's trajectory's timestep related
        u0_frames, u1_frames = len(self.ag0.universe.trajectory), len(self.ag1.universe.trajectory)
        frames_limit = min(u0_frames, u1_frames) #choose smaller

        if frame < frames_limit:
            self.ag0.universe.trajectory[frame] #goto frame
            self.ag1.universe.trajectory[frame] #goto frame

            ag0_com = self.ag0.atoms.center(self.weights)
            ag0_coordinates = self.ag0.atoms.positions - ag0_com
            ag1_com = self.ag1.atoms.center(self.weights)
            ag1_coordinates = self.ag1.atoms.positions - ag1_com
            rotation, dump = align.rotation_matrix(ag0_coordinates,
                                                ag1_coordinates,
                                                weights=self.weights)
            vector = ag1_com
            if self.plane is not None:
                matrix = np.r_[rotation, np.zeros(3).reshape(1, 3)]
                matrix = np.c_[matrix, np.zeros(4)]
                euler_angs = np.asarray(euler_from_matrix(matrix, axes='sxyz'),
                                        np.float32)
                for i in range(0, euler_angs.size):
                    euler_angs[i] = (euler_angs[self.plane] if i == self.plane
                                    else 0)
                rotation = euler_matrix(euler_angs[0],
                                        euler_angs[1],
                                        euler_angs[2],
                                        axes='sxyz')[:3, :3]
                vector[self.plane] = ag0_com[self.plane]
            ts.positions = ts.positions - ag0_com
            ts.positions = np.dot(ts.positions, rotation.T)
            ts.positions = ts.positions + vector #ag0 aligned to ag1!
            return ts
        else:
            pass

logger = logging.getLogger(__name__)
class AngleCalculations(AnalysisBase):  # subclass AnalysisBase

    def __init__(self, atomgroup0: AtomGroup, atomgroup1: AtomGroup, atom_pairs: List[tuple], verbose=True, args=None, add_transformations=True, **kwargs):
        """
        Set up the initial analysis parameters.
        """
        # must first run AnalysisBase.__init__ and pass the trajectory
        assert isinstance(atomgroup0, AtomGroup) and isinstance(atomgroup1, AtomGroup), "input must be an AtomGroup instance"
        trajectory = atomgroup0.universe.trajectory
        super(AngleCalculations, self).__init__(trajectory,
                                               verbose=verbose)
        del self._trajectory #superclass self._trajectory is not needed!
        self.radian = kwargs.get("radian", True)

        # set atomgroup as a property for access in other methods
        self.atomgroup0 = atomgroup0
        self.atomgroup1 = atomgroup1
        assert len(self.atomgroup0) == len(self.atomgroup1), "ATOMS: number of atoms between two atom groups must be the same"
        # REMOVED: assert len(self.atomgroup0.universe.trajectory) == len(self.atomgroup1.universe.trajectory), "FRAMES: number of frames between two atom groups must be the same"
        atom_pairs: Union[List[str], List[Tuple[int]]] = list(map(lambda inp: ast.literal_eval(inp) if isinstance(inp, str) else inp, atom_pairs )) #to -> List[int]
        self.atom_pairs = np.array(atom_pairs) #(pairs, 2)
        self.add_transformations = add_transformations

        #Try
        #_fit_to
        #1. AlignTraj: https://docs.mdanalysis.org/2.2.0/_modules/MDAnalysis/analysis/align.html#alignto:~:text=mobile_atoms%2C%20self,(mobile_atoms)
        #2. PCA: https://docs.mdanalysis.org/2.2.0/_modules/MDAnalysis/analysis/pca.html#PCA:~:text=if%20self._calc_mean,self.mean)
        #3. MDA Transformations: https://docs.mdanalysis.org/2.2.0/_modules/MDAnalysis/transformations/fit.html#fit_translation 
        #3.reference: rotation to quaternion: https://docs.mdanalysis.org/2.2.0/_modules/MDAnalysis/transformations/fit.html#fit_translation:~:text=matrix%20%3D%20np.r_%5Brotation%2C%20np.zeros(3).reshape(1%2C%203)%5D%0A%20%20%20%20%20%20%20%20%20%20%20%20matrix%20%3D%20np.c_%5Bmatrix%2C%20np.zeros(4)%5D%0A%20%20%20%20%20%20%20%20%20%20%20%20euler_angs%20%3D%20np.asarray(euler_from_matrix(matrix%2C%20axes%3D%27sxyz%27)%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20np.float32)
        #3.ps MDA lib (quaternion) Transformation: https://docs.mdanalysis.org/2.2.0/documentation_pages/lib/transformations.html#module-MDAnalysis.lib.transformations
        ##PROCEDURE: qdpCalc -> rotation matrix (3x3) for alignment -> remove plane & quaternion based matrix for rot/trans (4x4)

        # we can calculate masses now because they do not depend
        # on the trajectory frame.
        # self.masses = self.atomgroup.masses
        # self.total_mass = np.sum(self.masses)

    def _setup_frames(self, trajectory, start=None, stop=None, step=None):
        """
        Pass a Reader object and define the desired iteration pattern
        through the trajectory

        Parameters
        ----------
        trajectory : mda.Reader
            A trajectory Reader
        start : int, optional
            start frame of analysis
        stop : int, optional
            stop frame of analysis
        step : int, optional
            number of frames to skip between each analysed frame


        .. versionchanged:: 1.0.0
            Added .frames and .times arrays as attributes
        """
        # self._trajectory = trajectory
        start, stop, step = trajectory.check_slice_indices(start, stop, step)
        self.start = start
        self.stop = stop
        self.step = step
        self.n_frames = len(range(start, stop, step))
        self.frames = np.zeros(self.n_frames, dtype=int)
        self.times = np.zeros(self.n_frames)
        # assert len(self.atomgroup0) == len(self.atomgroup1), "number of atoms must be the same..."

        self.n_atoms = len(self.atomgroup0)

        return trajectory

    def _prepare(self):
        """
        Create array of zeroes as a placeholder for results.
        This is run before we begin looping over the trajectory.
        """
        # This must go here, instead of __init__, because
        # it depends on the number of frames specified in run().
        self.results.angles = np.zeros((self.n_frames, self.atom_pairs.shape[0]))
        # We put in 6 columns: 1 for the frame index,
        # 1 for the time, 4 for the radii of gyration
        self.u0 = self.atomgroup0.universe 
        self.u1 = self.atomgroup1.universe 
        print("Single frame BEFORE: ", self.u0.trajectory.ts.positions, self.u1.trajectory.ts.positions) #trajectory has : frame, time, ts

        if self.add_transformations:
            transform = fit_rot_trans(self.atomgroup0, self.atomgroup1) #atomgroup0 and 1 at _frame_index;; TM regions only (i.e. args.align_selection)
            self.u0.trajectory.add_transformations(transform) #CAUSING ERRORS! when u0 and u1 have different lengths!
        else:
            pass
            
    def _single_frame(self):
        """
        This function is called for every frame that we choose
        in run().
        """
        row, col = self.atom_pairs[:,0], self.atom_pairs[:,1]
        row_ = " ".join(row.astype(str).tolist())
        col_ = " ".join(col.astype(str).tolist())

        self.u0.trajectory[self._frame_index] #go to _frame_index; aligned onto U2
        self.u1.trajectory[self._frame_index] #go to _frame_index; reference
        if self.u0.trajectory.frame == 0:
            print("Single frame AFTER: ", self.u0.trajectory.ts.positions, self.u1.trajectory.ts.positions)

        assert self.n_atoms == len(self.atomgroup0) and self.n_atoms == len(self.atomgroup1), "number of atoms have changed..."
        ag0_pos_r, ag0_pos_c = np.array([self.u0.atoms.select_atoms(f"index {r}").positions for r in row]), np.array([self.u0.atoms.select_atoms(f"index {c}").positions for c in col])
        ag1_pos_r, ag1_pos_c = np.array([self.u1.atoms.select_atoms(f"index {r}").positions for r in row]), np.array([self.u1.atoms.select_atoms(f"index {c}").positions for c in col])
        ag0_displacement = ag0_pos_r - ag0_pos_c #(npairs, 3)
        ag1_displacement = ag1_pos_r - ag1_pos_c #(npairs, 3)
        angles = np.array([mda.lib.mdamath.angle(disp0.reshape(-1, ), disp1.reshape(-1, )) for disp0, disp1 in zip(ag0_displacement, ag1_displacement)])

        self.results.angles[self._frame_index, :] = angles.reshape(-1, )

    def _conclude(self):
        """
        Finish up by calculating an average and transforming our
        results into a DataFrame.
        """
        # by now self.result is fully populated
        print("Angle alculation is done!")
        self.results.angles = np.rad2deg(self.results.angles.copy()) if not self.radian else self.results.angles.copy()

    def run(self, start=None, stop=None, step=None, verbose=None):
        """Perform the calculation

        Parameters
        ----------
        start : int, optional
            start frame of analysis
        stop : int, optional
            stop frame of analysis
        step : int, optional
            number of frames to skip between each analysed frame
        verbose : bool, optional
            Turn on verbosity
        """
        logger.info("Choosing frames to analyze")
        # if verbose unchanged, use class default
        verbose = getattr(self, '_verbose',
                          False) if verbose is None else verbose

        self._trajectory0 = self._setup_frames(self.atomgroup0.universe.trajectory, start, stop, step)
        self._trajectory1 = self._setup_frames(self.atomgroup1.universe.trajectory, start, stop, step)

        logger.info("Starting preparation")
        self._prepare()
        for i, ts in enumerate(ProgressBar(
                self._trajectory0[self.start:self.stop:self.step],
                verbose=verbose)):
            self._frame_index = i
            self._ts = ts
            self.frames[i] = ts.frame
            self.times[i] = ts.time
            # logger.info("--> Doing frame {} of {}".format(i+1, self.n_frames))
            self._single_frame()
        logger.info("Finishing up")
        self._conclude()
        return self

def SASACalculations(prot_traj: mdtraj.Trajectory):
    #UNITS in nm
    #https://github.com/mdtraj/mdtraj/blob/8e85405586e549109f8865046867e1fda7f580d6/mdtraj/geometry/src/sasa.cpp#L92
    sasa = mdtraj.shrake_rupley(prot_traj, mode="residue").sum(axis=-1) #->(nframes, natoms/nresidues) -> (nframes, )
    return sasa

if __name__ == "__main__":
    ...
    #MDA units : Angstrom
    #MDTraj units: nm